#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_event.h"

#include "common.h"
#include "main.h"
#include "wifi.h"
#include "ble.h"
#include "nvs_memory.h"
#include "utils.h"
#include "thingsboard.h"
#include "http_ota.h"
#include "driver/gpio.h"

#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "esp_vfs_fat.h"
#include "driver/uart.h"
#include "sntp_lib.h"
#include "comm_uart.h"
#include "driver/timer.h"
#include "ds1307.h"


#define TIMER_DIVIDER         16  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER)  // convert counter value to seconds

#define GPIO_OUTPUT_IO_WDT    14
#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_OUTPUT_IO_WDT)

#define CFG_UART_RXD 16
#define CFG_UART_TXD 17

#define CFG_UART_RTS (UART_PIN_NO_CHANGE)
#define CFG_UART_CTS (UART_PIN_NO_CHANGE)

#define CFG_UART_PORT_NUM      			(2)
#define CFG_UART_BAUD_RATE     			(115200)
#define CFG_UART_TASK_STACK_SIZE    	(2048)

#define CFG_UART_BUF_SIZE (1024)

#define DS1307_SDA 18
#define DS1307_SCL 19


/* Event group to wifi event*/
EventGroupHandle_t system_event_group;
app_config_t app_config;
char provision_time[TIMESTAMP_MAX_LEN];
attributes_t app_attr;
sensor_data_t sensor_data;
mcu_fw_status_t mcu_fw_status;
static volatile uint32_t timer_counter = 0;
uint8_t mcu_fw_version = 0;
mc_error_t mc_errors;
bool system_time_updated = false;
bool is_firmware_update_successfully = false;


static void uart_configure_task (void* arg) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_LOGI("MAIN", "start uart_configure_task\n");

	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	ESP_ERROR_CHECK(uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM, 1024, 0, 0, NULL, 0));
	esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);
	esp_vfs_dev_uart_port_set_rx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CRLF);
	esp_vfs_dev_uart_port_set_tx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM, ESP_LINE_ENDINGS_CRLF);
	char chr[1000];

	vTaskDelay(5000 / portTICK_PERIOD_MS);

	while (true) {
		memset(chr, 0, 1000);
		printf("Enter Data : ");
		scanf("%s", chr);
		printf("\nData entered : %s.\n", chr);

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

static void fw_update_status_task (void* arg) {
	vTaskDelay(3000 / portTICK_PERIOD_MS);
	ESP_LOGI("MAIN", "start update_fw_update_status_task\n");
	esp_err_t err;

	// request update renesas
	if (mcu_fw_status.mcufw_need_update == 1) {
		uart_comm_msg_t comm_msg;
		comm_msg.type = UART_COMM_MSG_CMD;
		comm_msg.id = (uint16_t) (esp_random() & 0xffff);
		comm_msg.cmd = UPDATE_FW_REQUEST_CMD;
		uart_comm_send_msg(comm_msg);
	}
	else {
		xEventGroupSetBits(system_event_group, FW_UPDATE_DONE_BIT);
	}

	// update fw_update status if needed
	if (app_attr.is_update_fw == 1) {
		// wait for establish connected before send message
		while (true) {
			if (isThingsboardConnEstablished() == ESP_OK) {
				break;
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
		}

		// update fw_update status
		cJSON *fw_root = cJSON_CreateObject();
		cJSON_AddFalseToObject(fw_root, "isUpdateFw");
		cJSON_AddStringToObject(fw_root, "fwVersion", app_attr.fw_version);

		char *fw_root_string = cJSON_PrintUnformatted(fw_root);
		err = thingsboard_publish_attribute(fw_root_string);
		if (err != ESP_OK) {
			ESP_LOGE("MAIN", "thingsboard_publish_attribute for isUpdateFw error!");
		}

		vTaskDelay(100 / portTICK_PERIOD_MS);
		cJSON_Delete(fw_root);

		// clear is_update_fw flag
		app_attr.is_update_fw = 0;
		if (nvs_memory_set_attributes(&app_attr) != ESP_OK) {
			ESP_LOGE("MAIN", "nvs_memory_set_attributes: app_attr fail!");
		}
		vTaskDelay(100 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

static void sensor_publish_task (void* arg) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_LOGI("MAIN", "start sensor_publish_task\n");
	char strTmp[64];
	esp_err_t err;
	uint32_t _error = 0;
	EventBits_t uxBits;


	while (true) {

		if (isThingsboardConnEstablished() == ESP_OK) {
			// wait flag
			uxBits = xEventGroupWaitBits(system_event_group, ONE_SECOND_EVENT | MACHINE_ERROR_EVENT, pdTRUE, pdFALSE, portMAX_DELAY);

//			uint32_t free_heap_size=0, min_free_heap_size=0;
//			free_heap_size = esp_get_free_heap_size();
//			min_free_heap_size = esp_get_minimum_free_heap_size();
//			ESP_LOGI("HEAP","free heap size = %d \t  min_free_heap_size = %d",free_heap_size,min_free_heap_size);

			if ((uxBits & ONE_SECOND_EVENT) != 0x00U) {
				timer_counter++;
				if (timer_counter < app_attr.threshold) {
					continue;
				}
				else {
					timer_counter = 0;
				}
			}
			else if ((uxBits & MACHINE_ERROR_EVENT) != 0x00U) {
				timer_counter = 0;
			}
			else {
				timer_counter = 0;
			}


			if (xSensorDataSemaphore != NULL) {
				if( xSemaphoreTake( xSensorDataSemaphore, 100/portTICK_PERIOD_MS ) == pdTRUE )
				{
					// publish telemetry
					cJSON *root = cJSON_CreateObject();
					// rssi
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%d", wifi_info->rssi);
					cJSON_AddStringToObject(root, "rssi", strTmp);
					// ip_public
					cJSON_AddStringToObject(root, "ipPublic", wifi_info->public_ipaddr);
					// ip_local
					cJSON_AddStringToObject(root, "ipLocal", wifi_info->conn_info.ip_addr);
					// purityWaterOut
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.purity_water_out);
					cJSON_AddStringToObject(root, "purityWaterOut", strTmp);
					// purityWaterIn
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.purity_water_in);
					cJSON_AddStringToObject(root, "purityWaterIn", strTmp);
					// remainTimeFilter1
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.filter1_remain_time);
					cJSON_AddStringToObject(root, "remainTimeFilter1", strTmp);
					// remainTimeFilter2
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.filter2_remain_time);
					cJSON_AddStringToObject(root, "remainTimeFilter2", strTmp);
					// remainTimeFilter3
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.filter3_remain_time);
					cJSON_AddStringToObject(root, "remainTimeFilter3", strTmp);
					// remainTimeFilter4
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.filter4_remain_time);
					cJSON_AddStringToObject(root, "remainTimeFilter4", strTmp);
					// remainTimeFilter5
					memset(strTmp, 0, 64);
					sprintf(strTmp, "%u", sensor_data.filter5_remain_time);
					cJSON_AddStringToObject(root, "remainTimeFilter5", strTmp);

					_error = sensor_data.error;

					// release mutex
					xSemaphoreGive( xSensorDataSemaphore );


					if (sensor_data.error_changed != 0) {
						// construct error code string
						cJSON *error_root = cJSON_AddArrayToObject(root, "error");

						if (_error != 0) {
							if (_error & 0x01) {															// canh bao chat luong nuoc dau vao khong dam bao
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00051"));
							}
							if (_error & 0x02) {															// loi chat luong nuoc dau ra khong dam bao
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00052"));
							}
							if (_error & 0x04) {															// loi ro nuoc
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00053"));
							}
							if (_error & 0x08) {															// loi can nuoc trong binh chua
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00054"));
							}
							if (_error & 0x10) {															// loi bom RO hoat dong qua 60p
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00055"));
							}
							if (_error & 0x20) {															// Canh bao loi loc 1 hoat dong qua 80% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00056"));
							}
							if (_error & 0x40) {															// Canh bao loi loc 2 hoat dong qua 80% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00057"));
							}
							if (_error & 0x80) {															// Canh bao loi loc 3 hoat dong qua 80% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00058"));
							}
							if (_error & 0x100) {															// Canh bao loi loc 4 hoat dong qua 80% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00059"));
							}
							if (_error & 0x200) {															// Canh bao loi loc 5 hoat dong qua 80% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00060"));
							}
							if (_error & 0x400) {															// Canh bao loi loc 1 hoat dong qua 100% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00061"));
							}
							if (_error & 0x800) {															// Canh bao loi loc 2 hoat dong qua 100% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00062"));
							}
							if (_error & 0x1000) {															// Canh bao loi loc 3 hoat dong qua 100% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00063"));
							}
							if (_error & 0x2000) {															// Canh bao loi loc 4 hoat dong qua 100% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00064"));
							}
							if (_error & 0x4000) {															// Canh bao loi loc 5 hoat dong qua 100% tuoi tho
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00065"));
							}
							if (_error & 0x8000) {															// Canh bao chat luong nuoc dau ra
								cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00066"));
							}
						}
						else {
							cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00000"));				// No error/warning
						}

						// clear error_changed flag
						sensor_data.error_changed = 0;

						// clear mc_errors flag
						mc_errors.error_sync = 1;

						// save to nvs
						if (nvs_memory_set_mc_errors(&mc_errors) != ESP_OK) {
							ESP_LOGE("MAIN", "nvs_memory_set_mc_errors: fail!");
						}
					}

					char *root_string = cJSON_PrintUnformatted(root);

					ESP_LOGI("MAIN", "sensor_string: %s\n", root_string);

					err = thingsboard_publish_telemetry(root_string);
					if (err != ESP_OK) {
						ESP_LOGE("Main", "thingsboard_publish_telemetry for sensor_data error!");
					}

					vTaskDelay(100 / portTICK_PERIOD_MS);
					cJSON_Delete(root);
				}
				else {
					ESP_LOGE("MAIN", "ERROR! Cannot take xSensorDataSemaphore");
				}
			}
			else {
				ESP_LOGE("MAIN", "ERROR! Invalid xSensorDataSemaphore");
			}
		}

		vTaskDelay(100 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

static bool IRAM_ATTR timer_group_isr_callback(void *args)
{
    BaseType_t high_task_awoken = pdFALSE;
    timer_info_t *info = (timer_info_t *) args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    if (!info->auto_reload) {
        timer_counter_value += info->alarm_interval * TIMER_SCALE;
        timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);
    }

    if (info->timer_group == TIMER_GROUP_0) {
    	if (info->timer_idx == TIMER_0) {
    		portBASE_TYPE task_woken = pdFALSE;
    		xEventGroupSetBitsFromISR(system_event_group, ONE_SECOND_EVENT, &task_woken);
    	}
    	else if (info->timer_idx == TIMER_1) {
    		portBASE_TYPE task_woken = pdFALSE;
    		xEventGroupSetBitsFromISR(system_event_group, ONE_HOUR_EVENT, &task_woken);
    	}
    }

    //ESP_LOGI("TIMER", "OUT...");
    return high_task_awoken == pdTRUE; // return whether we need to yield at the end of ISR
}

void tg_timer_init(int group, int timer, bool auto_reload, int timer_interval_sec)
{
    /* Select and initialize basic parameters of the timer */
    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = auto_reload,
    }; // default clock source is APB
    timer_init(group, timer, &config);

    /* Timer's counter will initially start from value below.
       Also, if auto_reload is set, this value will be automatically reload on alarm */
    timer_set_counter_value(group, timer, 0);

    /* Configure the alarm value and the interrupt on alarm. */
    timer_set_alarm_value(group, timer, timer_interval_sec * TIMER_SCALE);
    timer_enable_intr(group, timer);

    timer_info_t *timer_info = calloc(1, sizeof(timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->auto_reload = auto_reload;
    timer_info->alarm_interval = timer_interval_sec;
    timer_isr_callback_add(group, timer, timer_group_isr_callback, timer_info, 0);

    timer_start(group, timer);
}

static void ds1307_task(void *pvParameters)
{
    i2c_dev_t dev;
    memset(&dev, 0, sizeof(i2c_dev_t));
    struct tm rtcTime, timeinfo;
    time_t now;
    esp_err_t err;
    time_t rtcnow;
    struct timeval tv;
    int ret;

    ESP_ERROR_CHECK(ds1307_init_desc(&dev, 0, DS1307_SDA, DS1307_SCL));

//    // setup datetime: 2018-04-11 00:52:10
//    struct tm rtcTime = {
//        .tm_year = 118, //since 1900 (2018 - 1900)
//        .tm_mon  = 3,  // 0-based
//        .tm_mday = 11,
//        .tm_hour = 0,
//        .tm_min  = 52,
//        .tm_sec  = 10
//    };

    // check ds1307 is configured or not
    if (app_config.is_rtc_config == RTC_NOT_CONFIG) {
    	// wait ntp synchronized
    	while (sntp_get_sync_state() != TIME_SYNCHRONIZED) {
			vTaskDelay(1000 / portTICK_PERIOD_MS);
    	}

    	// update rtc time
    	while (1) {
    		time(&now);
			localtime_r(&now, &timeinfo);

			if (ds1307_set_time(&dev, &timeinfo) == ESP_OK) {
				// check time
				if (ds1307_get_time(&dev, &rtcTime) == ESP_OK) {
					// Is time set? If not, tm_year will be (1970 - 1900).
					if (rtcTime.tm_year < (2016 - 1900)) {
						ESP_LOGI("MAIN", "DS1307 Time is invalid. Retry to set time to ds1307 ...");
						vTaskDelay(1000 / portTICK_PERIOD_MS);
					}
					else {
						// valid rtc time, update app_config
						app_config.is_rtc_config = RTC_CONFIG;
						err = nvs_memory_set_app_config(&app_config);
						if (err != ESP_OK) {
							ESP_LOGE("MAIN", "Error! Fail to save app_config to nvs memory!\n");
						}

						ESP_LOGI("MAIN", "DS1307 Time is configured!");
						break;
					}
				}
				else {
					ESP_LOGE("MAIN", "Fail to get ds1307 time!");
					vTaskDelay(60*60*1000 / portTICK_PERIOD_MS);
				}
			}
			else {
				ESP_LOGE("MAIN", "Fail to set ds1307 time!");
				vTaskDelay(60*60*1000 / portTICK_PERIOD_MS);
			}
    	}
    }
    else {
    	// update system time if time is not synchronized
    	if (sntp_get_sync_state() != TIME_SYNCHRONIZED) {
    		if (ds1307_get_time(&dev, &rtcTime) == ESP_OK) {
    			// update 'rtcnow' variable with current time
    			rtcnow = mktime(&rtcTime);

    			// update timeval tv variable
    			tv.tv_sec = rtcnow;
    			tv.tv_usec = 0;

    			// set time of day
    			ret = settimeofday(&tv, NULL);

    			if (ret == 0) {
    				ESP_LOGI("MAIN", "System time is updated from RTC time.");
    				system_time_updated = true;

    				// verify
    				time(&now);
    				localtime_r(&now, &timeinfo);

    				ESP_LOGI("System Time","%04d-%02d-%02d %02d:%02d:%02d\n", timeinfo.tm_year + 1900 /*Add 1900 for better readability*/, timeinfo.tm_mon + 1,
    								timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
    			}
    			else {
    				ESP_LOGE("MAIN", "ERROR! Fail to update system time from RTC time.");
    			}
    		}
    	}
    }

    // check rtc time then update if different
    while (1)
    {
    	xEventGroupWaitBits(system_event_group, ONE_HOUR_EVENT | REQUEST_RTC_UPDATE_BIT, pdTRUE, pdFALSE, portMAX_DELAY);

        if (ds1307_get_time(&dev, &rtcTime) == ESP_OK) {
        	ESP_LOGI("RTC Time","%04d-%02d-%02d %02d:%02d:%02d\n", rtcTime.tm_year + 1900 /*Add 1900 for better readability*/, rtcTime.tm_mon + 1,
        	        	  	  rtcTime.tm_mday, rtcTime.tm_hour, rtcTime.tm_min, rtcTime.tm_sec);

        	if (sntp_get_sync_state() == TIME_SYNCHRONIZED) {
        		time(&now);
				localtime_r(&now, &timeinfo);

				// check if time is different
				if ((timeinfo.tm_year != rtcTime.tm_year) || (timeinfo.tm_mon != rtcTime.tm_mon) || (timeinfo.tm_mday != rtcTime.tm_mday) ||
					(timeinfo.tm_hour != rtcTime.tm_hour) || (timeinfo.tm_mon != rtcTime.tm_mon) || (timeinfo.tm_min != rtcTime.tm_min))
				{
					// update rtc time
					if (ds1307_set_time(&dev, &timeinfo) == ESP_OK) {
						ESP_LOGI("RTC Time Updated","%04d-%02d-%02d %02d:%02d:%02d\n", timeinfo.tm_year + 1900 /*Add 1900 for better readability*/, timeinfo.tm_mon + 1,
											timeinfo.tm_mday, timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
					}
					else {
						ESP_LOGE("MAIN", "Fail to set ds1307 time!");
					}
				}
				else {
					ESP_LOGI("MAIN", "RTC time is up to date.");
				}
        	}
        	else {
        		ESP_LOGW("MAIN", "NTP time is NOT synchronized!");
        	}
        }
        else {
        	ESP_LOGE("MAIN", "Fail to get ds1307 time!");
        }
    }

    vTaskDelete(NULL);
}

static void send_old_data_task(void *pvParameters)
{
	ESP_LOGI("MAIN", "Starting send_old_data_task ...");
	esp_err_t err;

	while (isThingsboardConnEstablished() != ESP_OK) {
		vTaskDelay(5000 / portTICK_PERIOD_MS);
	}

	while (1) {
		xEventGroupWaitBits(system_event_group, SEND_OLD_DATA_REQUEST_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
		ESP_LOGI("TEST", "error_sync after: %d", mc_errors.error_sync);

		if (mc_errors.error_sync == 0) {
			// TODO: publish old error
			// publish old telemetry
			cJSON *root = cJSON_CreateObject();
			cJSON_AddStringToObject(root, "ts", mc_errors.time);
			cJSON *values_root = cJSON_AddObjectToObject(root, "values");
			cJSON *error_root = cJSON_AddArrayToObject(values_root, "error");

			if (mc_errors.error != 0) {
				if (mc_errors.error & 0x01) {															// canh bao chat luong nuoc dau vao khong dam bao
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00051"));
				}
				if (mc_errors.error & 0x02) {															// loi chat luong nuoc dau ra khong dam bao
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00052"));
				}
				if (mc_errors.error & 0x04) {															// loi ro nuoc
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00053"));
				}
				if (mc_errors.error & 0x08) {															// loi can nuoc trong binh chua
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00054"));
				}
				if (mc_errors.error & 0x10) {															// loi bom RO hoat dong qua 60p
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00055"));
				}
				if (mc_errors.error & 0x20) {															// Canh bao loi loc 1 hoat dong qua 80% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00056"));
				}
				if (mc_errors.error & 0x40) {															// Canh bao loi loc 2 hoat dong qua 80% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00057"));
				}
				if (mc_errors.error & 0x80) {															// Canh bao loi loc 3 hoat dong qua 80% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00058"));
				}
				if (mc_errors.error & 0x100) {															// Canh bao loi loc 4 hoat dong qua 80% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00059"));
				}
				if (mc_errors.error & 0x200) {															// Canh bao loi loc 5 hoat dong qua 80% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00060"));
				}
				if (mc_errors.error & 0x400) {															// Canh bao loi loc 1 hoat dong qua 100% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00061"));
				}
				if (mc_errors.error & 0x800) {															// Canh bao loi loc 2 hoat dong qua 100% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00062"));
				}
				if (mc_errors.error & 0x1000) {															// Canh bao loi loc 3 hoat dong qua 100% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00063"));
				}
				if (mc_errors.error & 0x2000) {															// Canh bao loi loc 4 hoat dong qua 100% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00064"));
				}
				if (mc_errors.error & 0x4000) {															// Canh bao loi loc 5 hoat dong qua 100% tuoi tho
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00065"));
				}
				if (mc_errors.error & 0x8000) {															// Canh bao chat luong nuoc dau ra
					cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00066"));
				}
			}
			else {
				cJSON_AddItemToArray(error_root, cJSON_CreateString("ER00000"));				// No error/warning
			}

			char *root_string = cJSON_PrintUnformatted(root);

			ESP_LOGI("MAIN", "old_data_string: %s\n", root_string);

			err = thingsboard_publish_telemetry(root_string);
			if (err != ESP_OK) {
				ESP_LOGE("Main", "thingsboard_publish_telemetry for old_data error!");
			}
			else {
				mc_errors.error_sync = 1;

				// save to nvs
				if (nvs_memory_set_mc_errors(&mc_errors) != ESP_OK) {
					ESP_LOGE("MAIN", "nvs_memory_set_mc_errors: fail!");
				}
			}

			vTaskDelay(100 / portTICK_PERIOD_MS);
			cJSON_Delete(root);
		}

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}


void app_main(void)
{
	system_time_updated = false;
	is_firmware_update_successfully = false;
	EventBits_t uxBits;

	/* Create default event loop needed by the
	 * main app and the provisioning service */
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	// init event group
	system_event_group = xEventGroupCreate();

	// init nvs memory
	//nvs_memory_erase();
	ESP_ERROR_CHECK(nvs_memory_init());

	// get app config from nvs memory
	ESP_ERROR_CHECK(nvs_memory_get_app_config(&app_config));

	// get app_attritue from nvs memory
	ESP_ERROR_CHECK(nvs_memory_get_attributes(&app_attr));

	// get fw version
	memset(app_attr.fw_version, 0, FW_VERSION_MAX_LEN);
	ESP_ERROR_CHECK(get_current_firmware_version(app_attr.fw_version));

	ESP_LOGI("FW_VERSION", "%s", app_attr.fw_version);

	// get mcu_fw_status
	ESP_ERROR_CHECK(nvs_memory_get_mcu_fw_status(&mcu_fw_status));

	// get mc_errors
	ESP_ERROR_CHECK(nvs_memory_get_mc_errors(&mc_errors));

	// init timer0 as one_second timer source
	tg_timer_init(TIMER_GROUP_0, TIMER_0, true, 1);

	// init timer1 as one_second timer source
	tg_timer_init(TIMER_GROUP_0, TIMER_1, true, 60*60);

	// init error in sensor data
	sensor_data.error = 0;
	sensor_data.error_changed = 0;

	// init uart communication
	uart_comm_init();

	// clear fw update done bit
	xEventGroupClearBits(system_event_group, FW_UPDATE_DONE_BIT);

	// fw_update_status task
	xTaskCreate(fw_update_status_task, "fw_update_status_task", 3*1024, NULL, 10, NULL);

	// wait for FW update bit done
	uxBits = xEventGroupWaitBits(system_event_group, FW_UPDATE_DONE_BIT, pdTRUE, pdFALSE, 30000 / portTICK_PERIOD_MS);

	if ((uxBits & FW_UPDATE_DONE_BIT) != 0x00U) {
		// update flag
		mcu_fw_status.mcufw_need_update = 0;
		if (nvs_memory_set_mcu_fw_status(&mcu_fw_status) != ESP_OK) {
			ESP_LOGE("MAIN", "nvs_memory_set_attributes: mcu_fw_status fail!");
		}
		vTaskDelay(100 / portTICK_PERIOD_MS);
	}

	// rtc init
	ESP_ERROR_CHECK(i2cdev_init());
	xTaskCreate(ds1307_task, "ds1307_task", 1024*2, NULL, 10, NULL);

	// init ota
	http_ota_init();

//	// uart configure task
//	xTaskCreate(uart_configure_task, "uart_configure_task", 5*1024, NULL, 10, NULL);
//
	// sensor publishing task
	xTaskCreate(sensor_publish_task, "sensor_publish_task", 3*1024, NULL, 10, NULL);

	// send old data task
	xTaskCreate(send_old_data_task, "send_old_data_task", 3*1024, NULL, 10, NULL);

	wifi_init();
	//ESP_LOGI("MAIN", "wifi_init: %d\n", err);
	//vTaskDelay(1000 / portTICK_PERIOD_MS);
	//err = wifi_reset_provision();

	// init ble
	ESP_ERROR_CHECK(ble_init(app_config.ble_name));
	if (app_config.is_ble_enable == BLE_DISABLE) {
		ESP_ERROR_CHECK(ble_deinit());
	}

	thingsboard_init();

	sntp_lib_init();


	vTaskDelete(NULL);
}




