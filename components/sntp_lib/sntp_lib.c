#include "sntp_lib.h"
#include "esp_sntp.h"
#include <time.h>
#include <sys/time.h>
#include "esp_attr.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "common.h"

#define SNTP_TAG  "SNTP"

/* Variable holding number of times ESP32 restarted since first boot.
 * It is placed into RTC memory using RTC_DATA_ATTR and
 * maintains its value when ESP32 wakes from deep sleep.
 */
RTC_DATA_ATTR static int boot_count = 0;

static void obtain_time(void);
static void initialize_sntp(void);
static time_sync_t _time_sync_state = TIME_NOT_SYNC;

#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_CUSTOM
void sntp_sync_time(struct timeval *tv)
{
   settimeofday(tv, NULL);
   ESP_LOGI(TAG, "Time is synchronized from custom code");
   sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
}
#endif

void time_sync_notification_cb(struct timeval *tv)
{
    ESP_LOGI(SNTP_TAG, "Notification of a time synchronization event");
}

static void obtain_time(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    ESP_ERROR_CHECK(esp_netif_init());

    /**
     * NTP server address could be aquired via DHCP,
     * see LWIP_DHCP_GET_NTP_SRV menuconfig option
     */
#ifdef LWIP_DHCP_GET_NTP_SRV
    sntp_servermode_dhcp(1);
#endif

    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET) {
        ESP_LOGI(SNTP_TAG, "Waiting for system time to be set... ");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    time(&now);
    localtime_r(&now, &timeinfo);

    // update time sync flag
    _time_sync_state = TIME_SYNCHRONIZED;
    system_time_updated = true;
}

static void initialize_sntp(void)
{
    ESP_LOGI(SNTP_TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_setservername(1, "0.pool.ntp.org");
    sntp_setservername(2, "1.pool.ntp.org");
    sntp_setservername(3, "2.pool.ntp.org");
    sntp_setservername(4, "3.pool.ntp.org");

    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH
    sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
#endif
    sntp_init();
}

static void sntp_init_task(void *param) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_LOGI(SNTP_TAG, "Start sntp_init_task\n");

	// wait for internet is ready
	xEventGroupWaitBits(system_event_group, WIFI_CONNECTED_FOR_NTP_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
	ESP_LOGI(SNTP_TAG, "Internet is ready. Start ntp client to get time ...\n");

    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
    // Is time set? If not, tm_year will be (1970 - 1900).
    if (timeinfo.tm_year < (2016 - 1900)) {
        ESP_LOGI(SNTP_TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
        obtain_time();
        // update 'now' variable with current time
        time(&now);
    }

    // send event notify time is synchronize
    xEventGroupSetBits(system_event_group, NTP_SYNCHRONIZE_EVENT);


#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH
    else {
        // add 500 ms error to the current system time.
        // Only to demonstrate a work of adjusting method!
        {
            ESP_LOGI(TAG, "Add a error for test adjtime");
            struct timeval tv_now;
            gettimeofday(&tv_now, NULL);
            int64_t cpu_time = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;
            int64_t error_time = cpu_time + 500 * 1000L;
            struct timeval tv_error = { .tv_sec = error_time / 1000000L, .tv_usec = error_time % 1000000L };
            settimeofday(&tv_error, NULL);
        }
        ESP_LOGI(TAG, "Time was set, now just adjusting it. Use SMOOTH SYNC method.");
        obtain_time();
        // update 'now' variable with current time
        time(&now);
    }
#endif

//    char strftime_buf[64];
////    // Set timezone to Vietnam Time
////    setenv("TZ", "UTC-7", 1);
////    tzset();
//    localtime_r(&now, &timeinfo);
//    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
//#ifdef ESP_DEBUG_LOG
//    ESP_LOGI(SNTP_TAG, "The current date/time UTC0 is: %s", strftime_buf);
//#endif

//    if (sntp_get_sync_mode() == SNTP_SYNC_MODE_SMOOTH) {
//        struct timeval outdelta;
//        while (sntp_get_sync_status() == SNTP_SYNC_STATUS_IN_PROGRESS) {
//            adjtime(NULL, &outdelta);
//            ESP_LOGI(SNTP_TAG, "Waiting for adjusting time ... outdelta = %li sec: %li ms: %li us",
//                        (long)outdelta.tv_sec,
//                        outdelta.tv_usec/1000,
//                        outdelta.tv_usec%1000);
//
//            vTaskDelay(2000 / portTICK_PERIOD_MS);
//        }
//    }


    vTaskDelete(NULL);
}

void sntp_lib_init(void)
{
    ++boot_count;
    ESP_LOGI(SNTP_TAG, "Boot count: %d", boot_count);

    // create new task for init sntp_init_task
    xTaskCreate(sntp_init_task, "sntp_init_task", 3*1024, NULL, 10, NULL);
}

time_sync_t sntp_get_sync_state(void) {
	return _time_sync_state;
}

esp_err_t sntp_get_current_timestamp(char * timestamp) {
	if (_time_sync_state != TIME_SYNCHRONIZED) {
		return ESP_FAIL;
	}

	memset(timestamp, 0, TIMESTAMP_MAX_LEN);
	struct timeval tv_now;
	gettimeofday(&tv_now, NULL);

	int64_t time_ms = (int64_t)tv_now.tv_sec * 1000LL + (int64_t)tv_now.tv_usec / 1000LL;
	sprintf(timestamp, "%lld", time_ms);

	return ESP_OK;
}








