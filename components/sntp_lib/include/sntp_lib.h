#ifndef _SNTP_LIB_H_
#define _SNTP_LIB_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
	TIME_NOT_SYNC,
	TIME_SYNCHRONIZED
} time_sync_t;



void sntp_lib_init(void);
time_sync_t sntp_get_sync_state(void);
esp_err_t sntp_get_current_timestamp(char * timestamp);


#ifdef __cplusplus
}
#endif

#endif /* _SNTP_LIB_H_ */
