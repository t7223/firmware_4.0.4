#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINES
 ****************************************************************************************
 */

#define CONFIG_ESP_COMM_UART_NUM 0

#define DEVICE_NOT_PROVISIONED	0
#define DEVICE_PROVISIONED		1

#define RTC_NOT_CONFIG	0
#define RTC_CONFIG		1

#define BLE_MTU_SIZE	500
#define BLE_DISABLE		0
#define BLE_ENABLE		1

#define TIMESTAMP_MAX_LEN		16
#define FW_VERSION_MAX_LEN		32

#define UART_COMM_MSG_CMD 0x01
#define UART_COMM_MSG_RESP 0x02
#define UART_COMM_MSG_STATE 0x03
#define UART_COMM_MSG_FW	0x04

#define UART_COMM_RESP_OK		0x01
#define UART_COMM_RESP_ERROR	0x02



/* For system_event_group BIT defined
 * - WIFI_CONNECTED_BIT we are connected to the AP with an IP
 * - WIFI_DISCONNECTED_BIT when disconnected from AP
 * */
#define WIFI_CONNECTED_BIT 							BIT0
#define WIFI_DISCONNECTED_BIT 						BIT1
#define PROV_REQUEST_SUCCESS_EVENT					BIT2
#define PROV_REQUEST_FAIL_EVENT						BIT3
#define PROV_AUTHEN_SUCCESS_EVENT					BIT4
#define PROV_AUTHEN_FAIL_EVENT						BIT5
#define PROV_RESTART_EVENT							BIT6
#define PROV_CONNECTED_EVENT						BIT7
#define INTERNET_READY_EVENT						BIT8
#define NTP_SYNCHRONIZE_EVENT						BIT9
#define ONE_SECOND_EVENT							BIT10
#define MACHINE_ERROR_EVENT							BIT11
#define REQUEST_RTC_UPDATE_BIT						BIT12
#define ONE_HOUR_EVENT								BIT13
#define WIFI_CONNECTED_FOR_NTP_BIT 					BIT14
#define GOT_MCU_FW_BIT 								BIT15
#define FW_UPDATE_DONE_BIT							BIT16
#define SEND_OLD_DATA_REQUEST_BIT					BIT17


#define FIRMWARE_VERSION "1.01"

#define MQTT_SUB_PROVISION_DEDAUT	"/provision/response"
#define MQTT_PUB_PROVISION_DEDAUT	"/provision/request"
#define MQTT_PUB_REGISTER_DEFAULT	"v1/devices/me/claim"
#define MQTT_SUB_CMD_TOPIC_DEFAULT "v1/devices/me/rpc/request/+"
#define MQTT_SUB_CMD_PRE_DEFAULT	"v1/devices/me/rpc/request/"
#define MQTT_PUB_CMDRESP_TOPIC_DEFAULT	"v1/devices/me/rpc/response/"
#define MQTT_PUB_TELEMETRY_TOPIC_DEFAULT "v1/devices/me/telemetry"
#define MQTT_PUB_TOPIC_DEFVAL	"v1/devices/me/attributes"
#define MQTT_BROKER_URL "platform-aiot.kangaroo.vn"
#define MQTT_BROKER_URI "mqtt://platform-aiot.kangaroo.vn"

// Device provisioning info
#define PROVISION_DEVICE_KEY "b98f8a99967e4c7b9086"
#define PROVISION_DEVICE_SECRET "80279400bc1f430b9a4b"
#define PROVISION_USERNAME "provision"
#define PROVISION_PASSWORD ""

// Device provisioning Topic info
#define PROVISION_TOPIC_REQUEST "/provision/request"
#define PROVISION_TOPIC_RESPONSE "/provision/response"



#define AP_RECONN_ATTEMPTS  		5

#define CONFIG_DEFAULT 				0x12038601
#define DEVICEID_DEFAULT 			"KGAML00099"
//#define DEVICEID_DEFAULT 			"KGAML10014"
#define BLE_NAME_DEFAULT			"KG100HED-IOT"
#define CREDENTIAL_TOKEN_DEFAULT	""

#define WIFI_CONFIGURED 		1
#define WIFI_NOT_CONFIGURED		0

#define DEVID_MAX_LEN 64
#define BLE_NAME_MAX_LEN 64
#define TOKEN_MAX_LEN 64
#define WIFI_SSID_MAX_LEN 33
#define WIFI_PASS_MAX_LEN 64

#define JSON_OBJ_MAX_LEN 500
#define FW_URL_MAX_LEN	400
#define FW_VERSION_MAX_LEN	32
#define FW_CKSUM_MAX_LEN	64

#define FW_VERSION_DEFAULT			"1.01"
#define MANUFACTURER_TIME_DEFAULT	"1636934400000"		// Monday, November 15, 2021 12:00:00 AM in GMT
#define FILTER1_TIME_DEFAULT		250
#define FILTER2_TIME_DEFAULT		500
#define FILTER3_TIME_DEFAULT		750
#define FILTER4_TIME_DEFAULT		2700
#define FILTER5_TIME_DEFAULT		1000
#define IS_UPDATE_FW_DEFAULT		0					// 0 or 1
#define TELE_THRESHOLD_DEFAULT		60




#define UART_COMM_MSG_PARAM_MAX_LEN		10

typedef struct {
	uint16_t delay;
	char data[BLE_MTU_SIZE];
} __attribute__((packed)) ble_notify_message_t;

typedef struct {
	uint32_t config_id;

	uint8_t is_provisioned;
	uint8_t is_ble_enable;
	char device_id[DEVID_MAX_LEN];
    char device_token[TOKEN_MAX_LEN];

    char ble_name[BLE_NAME_MAX_LEN];
    uint8_t is_rtc_config;

} __attribute__((packed)) app_config_t;

typedef struct {
	char manufacturer_time[TIMESTAMP_MAX_LEN];
	uint32_t threshold;						// tan suat ban telemetry in seconds
	char fw_version[FW_VERSION_MAX_LEN];
	uint32_t filter1_total_time;			// in hours
	uint32_t filter2_total_time;			// in hours
	uint32_t filter3_total_time;			// in hours
	uint32_t filter4_total_time;			// in hours
	uint32_t filter5_total_time;			// in hours
	uint8_t is_update_fw;	// updated firmware or not: 0 - not / 1 - updating fw

} __attribute__((packed)) attributes_t;

typedef struct {
	uint16_t purity_water_out;
	uint16_t purity_water_in;
	uint16_t filter1_remain_time;	// in hours
	uint16_t filter2_remain_time;	// in hours
	uint16_t filter3_remain_time;	// in hours
	uint16_t filter4_remain_time;	// in hours
	uint16_t filter5_remain_time;	// in hours
	uint32_t error;
	uint8_t error_changed;			// only send when error is changed

} __attribute__((packed)) sensor_data_t;

typedef struct {
	uint8_t type;
	uint16_t id;
	uint8_t cmd;
	uint8_t status;
	uint8_t params[UART_COMM_MSG_PARAM_MAX_LEN];

} __attribute__((packed)) uart_comm_msg_t;

typedef struct {
	uint8_t type;
	uint16_t filter1_hour;
	uint16_t filter2_hour;
	uint16_t filter3_hour;
	uint16_t filter4_hour;
	uint16_t filter5_hour;
	uint16_t tds_in;
	uint16_t tds_out;
	uint32_t error;

} __attribute__((packed)) uart_state_msg_t;


typedef struct {
	uint32_t error;
	char time[TIMESTAMP_MAX_LEN];

} __attribute__((packed)) old_error_msg_t;

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
    bool auto_reload;

} __attribute__((packed)) timer_info_t;

typedef struct {
    timer_info_t info;
    uint64_t timer_counter_value;

} __attribute__((packed)) timer_event_t;

typedef struct {
    char fwUrl[FW_URL_MAX_LEN];
    char fwVersion[FW_VERSION_MAX_LEN];
    char fwCksum[FW_CKSUM_MAX_LEN];

} __attribute__((packed)) ota_msg_t;

typedef struct {
    uint8_t mcufw_need_update;

} __attribute__((packed)) mcu_fw_status_t;

typedef struct {
	uint32_t error;
	char time[TIMESTAMP_MAX_LEN];
	uint8_t error_sync;
} __attribute__((packed)) mc_error_t;	// machine error

extern EventGroupHandle_t system_event_group;
extern app_config_t app_config;
extern char provision_time[];
extern attributes_t app_attr;
extern sensor_data_t sensor_data;
extern mcu_fw_status_t mcu_fw_status;
extern SemaphoreHandle_t xSensorDataSemaphore;
extern uint8_t mcu_fw_version;
extern mc_error_t mc_errors;
extern bool system_time_updated;
extern bool is_firmware_update_successfully;


#ifdef __cplusplus
}
#endif

#endif /* _COMMON_H_ */
