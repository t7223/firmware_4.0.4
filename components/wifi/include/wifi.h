#ifndef _WIFI_H_
#define _WIFI_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esp_wifi.h"
#include "esp_event.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include <lwip/inet.h>
#include "cJSON.h"

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NUM_AP	40







/**
 * @brief   WiFi STA status
 */
typedef enum {
    WIFI_STA_CONNECTING,
    WIFI_STA_CONNECTED,
    WIFI_STA_DISCONNECTED
} wifi_sta_state_t;

/**
 * @brief   WiFi STA connection fail reason
 */
typedef enum {
    WIFI_STA_AUTH_ERROR,
    WIFI_STA_AP_NOT_FOUND
} wifi_sta_fail_reason_t;

/**
 * @brief   WiFi STA connected status information
 */
typedef struct {
    /**
     * IP Address received by station
     */
    char    ip_addr[IP4ADDR_STRLEN_MAX];

    char    bssid[6];   /*!< BSSID of the AP to which connection was estalished */
    char    ssid[33];   /*!< SSID of the to which connection was estalished */
    uint8_t channel;    /*!< Channel of the AP */
    uint8_t auth_mode;  /*!< Authorization mode of the AP */
} wifi_sta_conn_info_t;


/**
 * @brief   Data relevant to provisioning application
 */
typedef struct {
    /* State of WiFi Station */
    wifi_sta_state_t wifi_state;

    /* Code for WiFi station disconnection (if disconnected) */
    wifi_sta_fail_reason_t wifi_disconnect_reason;

    /**
	 * Connection information (valid only when `wifi_state` is `WIFI_STATION_CONNECTED`)
	 */
	wifi_sta_conn_info_t   conn_info;

	char public_ipaddr[IP4ADDR_STRLEN_MAX];
	uint8_t internet_status;
	int8_t rssi;

} wifi_sta_app_info_t;



extern wifi_sta_app_info_t * wifi_info;


esp_err_t wifi_init(void);
esp_err_t wifi_uninit(void);
esp_err_t wifi_reset_provision(void);
esp_err_t wifi_is_provisioned(bool *provisioned);
esp_err_t wifi_start_with_credentials(const char *ssid, const char *passwd);
esp_err_t wifi_start_scan();



#ifdef __cplusplus
}
#endif

#endif /* _WIFI_H_ */
