#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include <sys/socket.h>
#include "lwip/dns.h"
#include "wifi.h"
#include "ble.h"
#include "nvs_memory.h"
#include "utils.h"
#include "comm_uart.h"


// Khi re-connect wifi thi co 2 cach
// 1. stop roi start de goi esp_wifi_connect o event WIFI_EVENT_STA_START
// 2. khong stop thi phai goi esp_wifi_connect trong ham
// o day minh dung phuong phap 1.

// note: wifi_reset_provision will stop wifi

#define WIFI_TAG  "WIFI"

wifi_sta_app_info_t * wifi_info;
static bool m_reconnect = true;	// used to prevent reconnect after disconnected.
QueueHandle_t ble_notify_queue = NULL;

static void event_handler(void* arg, esp_event_base_t event_base,
                          int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
    	wifi_info->wifi_state = WIFI_STA_CONNECTING;
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        wifi_event_sta_disconnected_t* disconnected = (wifi_event_sta_disconnected_t*) event_data;
        /* Station couldn't connect to configured host SSID */
        wifi_info->wifi_state = WIFI_STA_DISCONNECTED;
        xEventGroupSetBits(system_event_group, WIFI_DISCONNECTED_BIT);

        // notify renesas
        if (app_config.is_ble_enable == false) {
        	uart_comm_msg_t comm_msg;
			comm_msg.type = UART_COMM_MSG_CMD;
			comm_msg.id = (uint16_t) (esp_random() & 0xffff);
			comm_msg.cmd = WIRELESS_OFF_STATE_CMD;

			uart_comm_send_msg(comm_msg);
        }

        switch (disconnected->reason) {
			case WIFI_REASON_AUTH_EXPIRE:
			case WIFI_REASON_4WAY_HANDSHAKE_TIMEOUT:
			case WIFI_REASON_BEACON_TIMEOUT:
			case WIFI_REASON_AUTH_FAIL:
			case WIFI_REASON_ASSOC_FAIL:
			case WIFI_REASON_HANDSHAKE_TIMEOUT:
				ESP_LOGW(WIFI_TAG, "connect to the AP fail : auth Error");
				wifi_info->wifi_disconnect_reason = WIFI_STA_AUTH_ERROR;
				vTaskDelay(5000 / portTICK_PERIOD_MS);

				if (m_reconnect == true) {
					wifi_info->wifi_state = WIFI_STA_CONNECTING;
					esp_wifi_connect();
					ESP_LOGI(WIFI_TAG, "retry connecting to the AP...");
				}
				break;
			case WIFI_REASON_NO_AP_FOUND:
				ESP_LOGW(WIFI_TAG, "connect to the AP fail : not found");
				wifi_info->wifi_disconnect_reason = WIFI_STA_AP_NOT_FOUND;
				vTaskDelay(5000 / portTICK_PERIOD_MS);

				if (m_reconnect == true) {
					wifi_info->wifi_state = WIFI_STA_CONNECTING;
					esp_wifi_connect();
					ESP_LOGI(WIFI_TAG, "retry to connecting to the AP...");
				}
				break;
			default:
				/* None of the expected reasons */
				vTaskDelay(5000 / portTICK_PERIOD_MS);

				if (m_reconnect == true) {
					wifi_info->wifi_state = WIFI_STA_CONNECTING;
					esp_wifi_connect();
					ESP_LOGI(WIFI_TAG, "retry to connecting to the AP...");
				}
				break;
		}
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(WIFI_TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));

        /* IP Addr assigned to STA */
		esp_netif_ip_info_t ip_info;
		esp_netif_get_ip_info(esp_netif_get_handle_from_ifkey("WIFI_STA_DEF"), &ip_info);
		esp_ip4addr_ntoa(&ip_info.ip, wifi_info->conn_info.ip_addr, sizeof(wifi_info->conn_info.ip_addr));

		/* AP information to which STA is connected */
		wifi_ap_record_t ap_info;
		esp_wifi_sta_get_ap_info(&ap_info);
		memcpy(wifi_info->conn_info.bssid, (char *)ap_info.bssid, sizeof(ap_info.bssid));
		memcpy(wifi_info->conn_info.ssid,  (char *)ap_info.ssid,  sizeof(ap_info.ssid));
		wifi_info->conn_info.channel   = ap_info.primary;
		wifi_info->conn_info.auth_mode = ap_info.authmode;
		wifi_info->rssi = ap_info.rssi;

		// get public ip address
		memset(wifi_info->public_ipaddr, 0, IP4ADDR_STRLEN_MAX);
		esp_err_t err = get_public_ipaddress(wifi_info->public_ipaddr);
		if (err != ESP_OK) {
			ESP_LOGE(WIFI_TAG, "get_public_ipaddress error!\n");
		}
		else {
			xEventGroupSetBits(system_event_group, INTERNET_READY_EVENT);
		}

		xEventGroupSetBits(system_event_group, WIFI_CONNECTED_BIT);
		xEventGroupSetBits(system_event_group, WIFI_CONNECTED_FOR_NTP_BIT);

		// notify renesas
		uart_comm_msg_t comm_msg;
		comm_msg.type = UART_COMM_MSG_CMD;
		comm_msg.id = (uint16_t) (esp_random() & 0xffff);
		comm_msg.cmd = WIFI_CONNECTED_CMD;

		uart_comm_send_msg(comm_msg);

        /* Station got IP. That means configuration is successful. */
		wifi_info->wifi_state = WIFI_STA_CONNECTED;

		// notify ble client
		cJSON *wifi_ret_root = cJSON_CreateObject();
		cJSON_AddStringToObject(wifi_ret_root, "btRetConnWifi", "1");
		char *wifi_ret_string = cJSON_PrintUnformatted(wifi_ret_root);
		ble_notify_message_t bleMsg;
		memset(bleMsg.data, 0x0, BLE_MTU_SIZE);
		strcpy(bleMsg.data, wifi_ret_string);
		bleMsg.delay = 5;
		xQueueSend(ble_notify_queue, ( void * )&bleMsg, 1000/portTICK_PERIOD_MS);
		cJSON_Delete(wifi_ret_root);

    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_SCAN_DONE) {
    	uint16_t ap_count = 0;
		esp_wifi_scan_get_ap_num(&ap_count);
		if (ap_count == 0) {
			ESP_LOGD(WIFI_TAG, "Nothing AP found\n");
		}
		else {
			ESP_LOGD(WIFI_TAG, "Found %d APs\n", ap_count);
			if (ap_count > 10) {
				ap_count = 10;
				ESP_LOGI(WIFI_TAG, "Use the first 10 APs\n");
			}
			wifi_ap_record_t *ap_list = (wifi_ap_record_t *)malloc(sizeof(wifi_ap_record_t) * ap_count);
			if (ap_list == NULL) {
				ESP_LOGE(WIFI_TAG, "Malloc error! ap_list is NULL!\n");
			}
			else {
				esp_err_t ap_err = esp_wifi_scan_get_ap_records(&ap_count, ap_list);
				if (ap_err != ESP_OK) {
					ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_scan_get_ap_records!\n", esp_err_to_name(ap_err));
				}
				else {
					cJSON *root;
					cJSON *list_ssid;

					root = cJSON_CreateObject();
					list_ssid = cJSON_AddArrayToObject(root, "btRetSsid");

					for (uint16_t i = 0; i < ap_count; i++) {
						char *ssid = (char *) (ap_list + i)->ssid;

						if (strlen(ssid) > 0) {
							ESP_LOGI("WIFI_TAG", "ssid: %s\n", ssid);
							cJSON_AddItemToArray(list_ssid, cJSON_CreateString(ssid));
						}
					}

					char *list_ssid_string = cJSON_PrintUnformatted(root);
					ESP_LOGD(WIFI_TAG, "my_json_string: %s\n",list_ssid_string);
					ap_err = ble_send_data(list_ssid_string, strlen(list_ssid_string));
					if (ap_err != ESP_OK) {
						ESP_LOGE(WIFI_TAG, "ble_send_data error!\n");
					}

					//cJSON_Delete(list_ssid);
					cJSON_Delete(root);
				}


				free(ap_list);
			}
		}

    }
}

static void ble_notification_task(void *param) {
	ble_notify_message_t msg;
	esp_err_t err;

	ESP_LOGI(WIFI_TAG, "ble_notification_task started!\n");

	for(;;) {
		vTaskDelay(10 / portTICK_PERIOD_MS);

		if (ble_notify_queue != NULL) {
			if(xQueueReceive(ble_notify_queue, &msg, portMAX_DELAY) == pdTRUE) {
//#ifdef ESP_DEBUG_LOG
//				ESP_LOGI(WIFI_TAG, "ble message delay: %u", msg.delay);
//				ESP_LOGI(WIFI_TAG, "ble message data: %s", msg.data);
//#endif

				// wait for response event in msg.delay seconds
				vTaskDelay(msg.delay * 1000 / portTICK_PERIOD_MS);

				if (strlen(msg.data) > 0) {
					err = ble_send_data(msg.data, strlen(msg.data));

					if (err == ESP_OK) {
						ESP_LOGI(WIFI_TAG, "ble_send_data: %s\n",msg.data);
					}
					else {
						ESP_LOGE(WIFI_TAG, "ble_send_data error: %s\n", msg.data);
					}
				}
			}
		}
	}
}


esp_err_t wifi_init(void) {
	esp_err_t err;
	m_reconnect = true;	// accept reconnecting after disconnected

	// create queue
	ble_notify_queue = xQueueCreate(3, sizeof(ble_notify_message_t));

	// create task for handling mesage to ble client
	xTaskCreate(ble_notification_task, "ble_notification_task", 3*1024, NULL, 10, NULL);

	ip_addr_t dnsserver;
	dnsserver.type = IPADDR_TYPE_V4;
	dnsserver.u_addr.ip4.addr = 0x08080808; //8.8.8.8 dns
	dns_setserver(0, &dnsserver);
	dnsserver.u_addr.ip4.addr = 0x08080404; //8.8.4.4 dns
	dns_setserver(1, &dnsserver);

	wifi_info = (wifi_sta_app_info_t *) malloc(sizeof(wifi_sta_app_info_t));
	if (wifi_info == NULL) {
		ESP_LOGE(WIFI_TAG, "Error! Not enough free memory to malloc g_info!\n");
		return ESP_FAIL;
	}

	/* Initialize networking stack */
	err = esp_netif_init();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_netif_init!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	/* Initialize Wi-Fi including netif with default config */
	esp_netif_create_default_wifi_sta();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	err = esp_wifi_init(&cfg);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_init!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	/* Set our event handling */
	err = esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_event_handler_register!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, event_handler, NULL);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_event_handler_register!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	/* Check if device is provisioned */
	bool provisioned;
	if (wifi_is_provisioned(&provisioned) != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error getting device provisioning state");
		return ESP_FAIL;
	}

	/* Start Wi-Fi in station mode with credentials set during provisioning */
	err = esp_wifi_set_mode(WIFI_MODE_STA);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_set_mode!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_start();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_start!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	return ESP_OK;
}

esp_err_t wifi_start_with_credentials(const char *ssid, const char *passwd) {
	esp_err_t err;
	m_reconnect = true;	// accept reconnecting after disconnected
	uint8_t i;

	wifi_config_t wifi_config = {
			.sta = {
				/* Setting a password implies station will connect to all security modes including WEP/WPA.
				 * However these modes are deprecated and not advisable to be used. Incase your Access point
				 * doesn't support WPA2, these mode can be enabled by commenting below line */
			 .threshold.authmode = WIFI_AUTH_WPA2_PSK,

				.pmf_cfg = {
					.capable = true,
					.required = false
				},
			},
		};

	strlcpy((char *) wifi_config.sta.ssid, ssid, sizeof(wifi_config.sta.ssid));
	if (passwd) {
		strlcpy((char *) wifi_config.sta.password, passwd, sizeof(wifi_config.sta.password));
	}

	// if wifi is in connected state then disconnect it first
	if (wifi_info->wifi_state == WIFI_STA_CONNECTED) {
		err = esp_wifi_disconnect();
		if (err != ESP_OK) {
			ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_disconnect!\n", esp_err_to_name(err));
			return ESP_FAIL;
		}

		// wait disconnected for maximum 5s
		for (i = 0; i < 5; i++) {
			if (wifi_info->wifi_state == WIFI_STA_DISCONNECTED) {
				break;
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
		}
	}

	// stop wifi first
	err = esp_wifi_stop();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_stop!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	/* Start Wi-Fi in station mode with credentials set during provisioning */
	err = esp_wifi_set_mode(WIFI_MODE_STA);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_set_mode!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_set_config(WIFI_IF_STA, &wifi_config);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_set_config!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_start();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_start!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	return ESP_OK;
}

esp_err_t wifi_uninit(void) {
	esp_err_t err;

	// free g_info
	free(wifi_info);

	/* Remove event handler */
	esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, event_handler);
	esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, event_handler);

	err = esp_wifi_stop();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_stop!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_deinit();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_deinit!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	return ESP_OK;
}

esp_err_t wifi_is_provisioned(bool *provisioned) {
    *provisioned = false;

    /* Get WiFi Station configuration */
    wifi_config_t wifi_cfg;
    if (esp_wifi_get_config(WIFI_IF_STA, &wifi_cfg) != ESP_OK) {
        return ESP_FAIL;
    }

    if (strlen((const char*) wifi_cfg.sta.ssid)) {
        *provisioned = true;
        ESP_LOGI(WIFI_TAG, "Found ssid %s",     (const char*) wifi_cfg.sta.ssid);
        ESP_LOGI(WIFI_TAG, "Found password %s", (const char*) wifi_cfg.sta.password);
    }
    return ESP_OK;
}

esp_err_t wifi_reset_provision(void) {
	esp_err_t err;

	m_reconnect = false;	// not permit reconnecting after disconnected

	err = esp_wifi_stop();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_stop!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_restore();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_restore!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	/* Start Wi-Fi in station mode with credentials set during provisioning */
	err = esp_wifi_set_mode(WIFI_MODE_STA);
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_set_mode!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	err = esp_wifi_start();
	if (err != ESP_OK) {
		ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_start!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	return ESP_OK;
}

esp_err_t wifi_start_scan() {
	esp_err_t err;
	uint8_t i;
	bool is_disconnected = false;

	m_reconnect = false;	// not permit reconnecting after disconnected

	// if wifi is in connected state then disconnect it first
	if (wifi_info->wifi_state == WIFI_STA_CONNECTED) {
		err = esp_wifi_disconnect();
		if (err != ESP_OK) {
			ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_disconnect!\n", esp_err_to_name(err));
			return ESP_FAIL;
		}

		// wait disconnected for maximum 5s
		for (i = 0; i < 5; i++) {
			if (wifi_info->wifi_state == WIFI_STA_DISCONNECTED) {
				is_disconnected = true;
				break;
			}
			vTaskDelay(1000 / portTICK_PERIOD_MS);
		}
	}
	else {
		is_disconnected = true; // already disconnected state
	}

	if (is_disconnected == true) {
		wifi_scan_config_t scanConf;
		memset(&scanConf, 0, sizeof(scanConf));
		scanConf.ssid = NULL;
		scanConf.bssid = NULL;
		scanConf.channel = 0; // all channels
		scanConf.scan_type = WIFI_SCAN_TYPE_ACTIVE;
		scanConf.show_hidden = true;

		err = esp_wifi_scan_start(&scanConf, true);
		if (err != ESP_OK) {
			ESP_LOGE(WIFI_TAG, "Error (%s) esp_wifi_scan_start!\n", esp_err_to_name(err));
			return ESP_FAIL;
		}
		ESP_LOGI(WIFI_TAG, "Wifi scan started.\n");
		return ESP_OK;
	}
	else {
		ESP_LOGI(WIFI_TAG, "Error! Wifi is not in disconnected state.\n");
		return ESP_FAIL;
	}
}

