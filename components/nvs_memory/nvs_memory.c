#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_memory.h"

#define NVS_MEMORY_TAG  "MVS_MEMORY"


esp_err_t nvs_memory_erase(void) {
	esp_err_t err = nvs_flash_erase();
	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_flash_erase!\n", esp_err_to_name(err));
	}
	return err;
}

esp_err_t nvs_memory_init(void) {
	// Initialize NVS
	esp_err_t err = nvs_flash_init();

	if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_flash_init! Try to erase and re-init it!\n", esp_err_to_name(err));
		// NVS partition was truncated and needs to be erased
		// Retry nvs_flash_init
		err = nvs_flash_erase();
		if (err != ESP_OK) {
			return ESP_FAIL;
		}

		err = nvs_flash_init();
		if (err != ESP_OK) {
			return ESP_FAIL;
		}
	}

	return ESP_OK;
}

esp_err_t nvs_memory_deinit(void) {
	esp_err_t err = nvs_flash_deinit();
	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_memory_deinit!\n", esp_err_to_name(err));
	}
	return err;
}

esp_err_t nvs_memory_get_app_config(app_config_t *appconfig) {

	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;
	size_t len;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// get config_id
		err = nvs_get_u32(handle, "config_id", &appconfig->config_id);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				appconfig->config_id = CONFIG_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading config_id!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get is_provision
		err = nvs_get_u8(handle, "is_provisioned", &appconfig->is_provisioned);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				appconfig->is_provisioned = DEVICE_NOT_PROVISIONED;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading is_provisioned!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get is_ble_enable
		err = nvs_get_u8(handle, "is_ble_enable", &appconfig->is_ble_enable);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				appconfig->is_ble_enable = BLE_ENABLE;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading is_ble_enable!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get device_id
		len = DEVID_MAX_LEN;
		err = nvs_get_str(handle, "device_id", appconfig->device_id, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				strcpy(appconfig->device_id, DEVICEID_DEFAULT);
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading device_id!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get credential token
		len = TOKEN_MAX_LEN;
		err = nvs_get_str(handle, "device_token", appconfig->device_token, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				strcpy(appconfig->device_token, CREDENTIAL_TOKEN_DEFAULT);
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading device_token!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get ble name
		len = BLE_NAME_MAX_LEN;
		err = nvs_get_str(handle, "ble_name", appconfig->ble_name, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				strcpy(appconfig->ble_name, BLE_NAME_DEFAULT);
				strcat(appconfig->ble_name, appconfig->device_id);
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ble_name!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get is_rtc_config
		err = nvs_get_u8(handle, "is_rtc_config", &appconfig->is_rtc_config);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				appconfig->is_rtc_config = RTC_NOT_CONFIG;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading is_rtc_config!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_set_app_config(app_config_t *appconfig) {
	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// set config_id
		err = nvs_set_u32(handle, "config_id", appconfig->config_id);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing config_id!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set is_provisioned
		err = nvs_set_u8(handle, "is_provisioned", appconfig->is_provisioned);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing is_provisioned!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set is_ble_enable
		err = nvs_set_u8(handle, "is_ble_enable", appconfig->is_ble_enable);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing is_ble_enable!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set device_id
		err = nvs_set_str(handle, "device_id", appconfig->device_id);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing device_id!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set credential token
		err = nvs_set_str(handle, "device_token", appconfig->device_token);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing device_token!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set ble name
		err = nvs_set_str(handle, "ble_name", appconfig->ble_name);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ble_name!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set is_rtc_config
		err = nvs_set_u8(handle, "is_rtc_config", appconfig->is_rtc_config);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing is_rtc_config!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// commit update
		err = nvs_commit(handle);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_commit!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_get_provision_time(char *provTime) {

	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;
	size_t len;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// get provision time
		len = TIMESTAMP_MAX_LEN;
		err = nvs_get_str(handle, "prov_time", provTime, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				// not found return FAIL
				ret = ESP_FAIL;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading prov_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_set_provision_time(char *provTime) {
	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// set provision time
		err = nvs_set_str(handle, "prov_time", provTime);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing prov_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// commit update
		err = nvs_commit(handle);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_commit!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_get_attributes(attributes_t *attr) {

	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;
	size_t len;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// get manufacturer_time
		len = TIMESTAMP_MAX_LEN;
		err = nvs_get_str(handle, "manuf_time", attr->manufacturer_time, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				strcpy(attr->manufacturer_time, MANUFACTURER_TIME_DEFAULT);
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading manufacturer_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get threshold
		err = nvs_get_u32(handle, "threshold", &attr->threshold);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->threshold = TELE_THRESHOLD_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading threshold!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get filter1_total_time
		err = nvs_get_u32(handle, "ft1_time", &attr->filter1_total_time);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->filter1_total_time = FILTER1_TIME_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ft1_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get filter2_total_time
		err = nvs_get_u32(handle, "ft2_time", &attr->filter2_total_time);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->filter2_total_time = FILTER2_TIME_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ft2_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get filter3_total_time
		err = nvs_get_u32(handle, "ft3_time", &attr->filter3_total_time);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->filter3_total_time = FILTER3_TIME_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ft3_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get filter4_total_time
		err = nvs_get_u32(handle, "ft4_time", &attr->filter4_total_time);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->filter4_total_time = FILTER4_TIME_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ft4_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get filter5_total_time
		err = nvs_get_u32(handle, "ft5_time", &attr->filter5_total_time);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->filter5_total_time = FILTER5_TIME_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading ft5_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get is_update_fw
		err = nvs_get_u8(handle, "is_update_fw", &attr->is_update_fw);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				attr->is_update_fw = IS_UPDATE_FW_DEFAULT;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading is_update_fw!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_set_attributes(attributes_t *attr) {
	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// set manufacturer_time
		err = nvs_set_str(handle, "manuf_time", attr->manufacturer_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing manuf_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set threshold
		err = nvs_set_u32(handle, "threshold", attr->threshold);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing threshold!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set filter1_total_time
		err = nvs_set_u32(handle, "ft1_time", attr->filter1_total_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ft1_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set filter2_total_time
		err = nvs_set_u32(handle, "ft2_time", attr->filter2_total_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ft2_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set filter3_total_time
		err = nvs_set_u32(handle, "ft3_time", attr->filter3_total_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ft3_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set filter4_total_time
		err = nvs_set_u32(handle, "ft4_time", attr->filter4_total_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ft4_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set filter5_total_time
		err = nvs_set_u32(handle, "ft5_time", attr->filter5_total_time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing ft5_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set is_update_fw
		err = nvs_set_u8(handle, "is_update_fw", attr->is_update_fw);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing is_update_fw!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// commit update
		err = nvs_commit(handle);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_commit!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_get_mcu_fw_status(mcu_fw_status_t *mcuFwStatus) {

	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// get mcufw_need_update
		err = nvs_get_u8(handle, "mcuNeed_update", &mcuFwStatus->mcufw_need_update);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				mcuFwStatus->mcufw_need_update = 0;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading mcufw_need_update!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_set_mcu_fw_status(mcu_fw_status_t *mcuFwStatus) {
	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// set mcufw_need_update
		err = nvs_set_u8(handle, "mcuNeed_update", mcuFwStatus->mcufw_need_update);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing mcufw_need_update!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// commit update
		err = nvs_commit(handle);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_commit!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_get_mc_errors(mc_error_t *mcErrors) {

	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;
	size_t len;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// get errors
		err = nvs_get_u32(handle, "mc_error", &mcErrors->error);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				mcErrors->error = 0;		// default no error
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading threshold!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get error_time
		len = TIMESTAMP_MAX_LEN;
		err = nvs_get_str(handle, "mcerror_time", mcErrors->time, &len);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				strcpy(mcErrors->time, MANUFACTURER_TIME_DEFAULT);
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading mcerror_time!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		// get error_sync
		err = nvs_get_u8(handle, "error_sync", &mcErrors->error_sync);
		switch (err) {
			case ESP_OK:
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				mcErrors->error_sync = 1;
				break;
			default:
				ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) reading error_sync!\n", esp_err_to_name(err));
				ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}

esp_err_t nvs_memory_set_mc_errors(mc_error_t *mcErrors) {
	esp_err_t err, ret = ESP_OK;
	nvs_handle_t handle;

	// Open NVS
	err = nvs_open("appconfig", NVS_READWRITE, &handle);

	if (err != ESP_OK) {
		ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
		return ESP_FAIL;
	} else {
		// set error
		err = nvs_set_u32(handle, "mc_error", mcErrors->error);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing mc_error!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set error_time
		err = nvs_set_str(handle, "mcerror_time", mcErrors->time);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing manuf_time!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// set error_sync
		err = nvs_set_u8(handle, "error_sync", mcErrors->error_sync);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) writing error_sync!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		// commit update
		err = nvs_commit(handle);
		if (err != ESP_OK) {
			ESP_LOGE(NVS_MEMORY_TAG, "Error (%s) nvs_commit!\n", esp_err_to_name(err));
			ret = ESP_FAIL;
		}

		nvs_close(handle);	// close nvs
		return ret;
	}
}
