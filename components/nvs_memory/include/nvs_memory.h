#ifndef _NVS_MEMORY_H_
#define _NVS_MEMORY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nvs.h"
#include "nvs_flash.h"

#include "common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINES
 ****************************************************************************************
 */


esp_err_t nvs_memory_erase(void);
esp_err_t nvs_memory_init(void);
esp_err_t nvs_memory_deinit(void);

esp_err_t nvs_memory_get_app_config(app_config_t *appconfig);
esp_err_t nvs_memory_set_app_config(app_config_t *appconfig);

esp_err_t nvs_memory_get_provision_time(char *provTime);
esp_err_t nvs_memory_set_provision_time(char *provTime);
esp_err_t nvs_memory_get_attributes(attributes_t *attr);
esp_err_t nvs_memory_set_attributes(attributes_t *attr);

esp_err_t nvs_memory_get_mcu_fw_status(mcu_fw_status_t *mcuFwStatus);
esp_err_t nvs_memory_set_mcu_fw_status(mcu_fw_status_t *mcuFwStatus);

esp_err_t nvs_memory_get_mc_errors(mc_error_t *mcErrors);
esp_err_t nvs_memory_set_mc_errors(mc_error_t *mcErrors);


#ifdef __cplusplus
}
#endif

#endif /* _NVS_MEMORY_H_ */
