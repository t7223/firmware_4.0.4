#include "comm_uart.h"
#include "nvs_memory.h"
#include "thingsboard.h"
#include "ble.h"
#include "utils.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "sntp_lib.h"
#include <time.h>
#include <sys/time.h>


#define COMM_UART_TAG  "COMM_UART"

QueueHandle_t uart_cmd_data_send_queue = NULL;
QueueHandle_t uart_cmd_data_process_queue = NULL;
QueueHandle_t uart_state_data_process_queue = NULL;
SemaphoreHandle_t xSensorDataSemaphore = NULL;
uint16_t updateFwId = 0;

extern const uint8_t firmware_bin_start[] asm("_binary_firmware_bin_start");
extern const uint8_t firmware_bin_end[] asm("_binary_firmware_bin_end");
extern const uint16_t firmware_bin_length asm("firmware_bin_length");

static uint16_t binaryBinCrc16 = 0x0000;


void get_current_timestamp(char * timestamp) {
	memset(timestamp, 0, TIMESTAMP_MAX_LEN);
	struct timeval tv_now;
	gettimeofday(&tv_now, NULL);

	int64_t time_ms = (int64_t)tv_now.tv_sec * 1000LL + (int64_t)tv_now.tv_usec / 1000LL;
	sprintf(timestamp, "%lld", time_ms);
}

static void uart_comm_tx_task(void *arg)
{
	uart_comm_msg_t msg;
	uint8_t data[COMM_UART_MAX_MSG_LEN];
	uint16_t i, j, len, txBytes, data_len, count, remaining;
	uint8_t crc8;

	for(;;) {
			vTaskDelay(10 / portTICK_PERIOD_MS);

			if (uart_cmd_data_send_queue != NULL) {
				if(xQueueReceive(uart_cmd_data_send_queue, &msg, portMAX_DELAY) == pdTRUE) {
					ESP_LOGI(COMM_UART_TAG, "Prepare to send msg with msg_type: %d, msg_id: %d and cmd_id: %d\n", msg.type, msg.id, msg.cmd);

					if (msg.type == UART_COMM_MSG_FW) {	// send firmware
						ESP_LOGI(COMM_UART_TAG, "Begin to send firmware ...");
						count = firmware_bin_length / COMM_UART_MAX_MSG_LEN;
						for (i = 0; i < count; i++) {
							memcpy(data, (firmware_bin_start + i*COMM_UART_MAX_MSG_LEN), COMM_UART_MAX_MSG_LEN);
							txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, COMM_UART_MAX_MSG_LEN);
							if (txBytes != COMM_UART_MAX_MSG_LEN) {
								ESP_LOGE(COMM_UART_TAG, "Invalid tx bytes sent! sent: %d, got: %d", COMM_UART_MAX_MSG_LEN, txBytes);
							}
							ESP_LOGI("FW Sending", "%d", i);
							vTaskDelay(200 / portTICK_PERIOD_MS);
						}
						// remaining
						remaining = firmware_bin_length - COMM_UART_MAX_MSG_LEN * count;
						if (remaining > 0) {
							memset(data, 0, COMM_UART_MAX_MSG_LEN);
							memcpy(data, (firmware_bin_start + count*COMM_UART_MAX_MSG_LEN), remaining);
							txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, remaining);
							if (txBytes != remaining) {
								ESP_LOGE(COMM_UART_TAG, "Invalid tx bytes sent! sent: %d, got: %d", remaining, txBytes);
							}
						}
						else {
							ESP_LOGI(COMM_UART_TAG, "No Extra data!");
						}
					}
					else {
						// prepare data to send
						memset(data, 0, COMM_UART_MAX_MSG_LEN);
						i = 0;
						data_len = 0;
						data[i++] = COMM_UART_HEADER;
						i = i+1;
						data[i++] = msg.type;
						data_len = data_len + 1;
						data[i++] = (uint8_t) (msg.id >> 8);
						data_len = data_len + 1;
						data[i++] = (uint8_t) (msg.id & 0xff);
						data_len = data_len + 1;

						if (msg.type == UART_COMM_MSG_RESP) {
							data[i++] = msg.status;
							data_len = data_len + 1;
							len = i;
							// update data len
							data[1] = data_len;

							crc8 = crc8_lsb(CRC8_POLY, data, len);
							data[len] = crc8;
							txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

							ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
							ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
						}
						else if (msg.type == UART_COMM_MSG_CMD) {
							if ((msg.cmd == RESTART_DEVICE_CMD) || (msg.cmd == WIFI_CONNECTED_CMD) || (msg.cmd == SERVER_CONNECTED_CMD)
							|| (msg.cmd == WIRELESS_OFF_STATE_CMD))
							{
								data[i++] = msg.cmd;
								data_len = data_len + 1;
								len = i;
								// update data len
								data[1] = data_len;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
							}
							else if (msg.cmd == SYNC_TIME_CMD) {
								data[i++] = msg.cmd;
								data_len = data_len + 1;
								// 4 bytes timestamp
								for (j = 0; j < 4; j++) {
									data[i++] = msg.params[j];
									data_len = data_len + 1;
								}
								len = i;
								// update data len
								data[1] = data_len;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
							}
							else if (msg.cmd == CONFIG_UPDATE_STATE_FREQ_CMD) {
								data[i++] = msg.cmd;
								data_len = data_len + 1;
								// 2 bytes freq in seconds
								for (j = 0; j < 2; j++) {
									data[i++] = msg.params[j];
									data_len = data_len + 1;
								}
								len = i;
								// update data len
								data[1] = data_len;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
							}
							else if ((msg.cmd == RESET_FILTER1_COUNTER_CMD) || (msg.cmd == RESET_FILTER2_COUNTER_CMD) || (msg.cmd == RESET_FILTER3_COUNTER_CMD)
							|| (msg.cmd == RESET_FILTER4_COUNTER_CMD) || (msg.cmd == RESET_FILTER5_COUNTER_CMD))
							{
								data[i++] = msg.cmd;
								data_len = data_len + 1;
								// 2 bytes lifetimes in hours, default using 0000
								for (j = 0; j < 2; j++) {
									data[i++] = msg.params[j];
									data_len = data_len + 1;
								}
								len = i;
								// update data len
								data[1] = data_len;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
							}
							else if (msg.cmd == GET_CURR_FW_VERSION_CMD) {
								data[i++] = msg.cmd;
								data_len = data_len + 1;

								len = i;
								// update data len
								data[1] = data_len;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);

							}
							else if (msg.cmd == UPDATE_FW_REQUEST_CMD) {
								data[i++] = msg.cmd;
								data_len = data_len + 1;
								// 2 bytes size
								data[i++] = (uint8_t)((firmware_bin_length >> 8) & 0xff);
								data_len = data_len + 1;
								data[i++] = (uint8_t)(firmware_bin_length & 0xff);
								data_len = data_len + 1;
								// 2 bytes crc16 of binary bin
								data[i++] = (uint8_t)((binaryBinCrc16 >> 8) & 0xff);
								data_len = data_len + 1;
								data[i++] = (uint8_t)(binaryBinCrc16 & 0xff);
								data_len = data_len + 1;

								len = i;
								// update data len
								data[1] = data_len;

								// update fw id
								updateFwId = msg.id;

								crc8 = crc8_lsb(CRC8_POLY, data, len);
								data[len] = crc8;
								txBytes = uart_write_bytes(COMM_UART_PORT_NUM, data, len+1);

								ESP_LOGD(COMM_UART_TAG, "Wrote %d bytes", txBytes);
								ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, len+1, ESP_LOG_DEBUG);
							}
							else {
								ESP_LOGE(COMM_UART_TAG, "ERROR! Invalid uart comm cmd");
							}
						}
						else {
							ESP_LOGE(COMM_UART_TAG, "Invalid sending message type: %d", msg.type);
						}
					}



				}
			}
	}

	vTaskDelete(NULL);
}

static void uart_comm_rx_task(void *arg)
{
    uint8_t data[COMM_UART_BUF_SIZE+1];
    uint8_t crc8;
    uint8_t i;
    uint16_t msg_id;

    while (1) {
    	memset(data, 0x00, COMM_UART_BUF_SIZE+1);
        const int rxBytes = uart_read_bytes(COMM_UART_PORT_NUM, data, COMM_UART_BUF_SIZE, 15/portTICK_RATE_MS);
        if (rxBytes > 0) {
        	ESP_LOGD(COMM_UART_TAG, "Read %d bytes: '%s'", rxBytes, data);
            ESP_LOG_BUFFER_HEXDUMP(COMM_UART_TAG, data, rxBytes, ESP_LOG_INFO);

            // check conditions
            if (rxBytes > 3) {
            	if (data[0] == COMM_UART_HEADER) {
					crc8 = crc8_lsb(CRC8_POLY, data, rxBytes-1);

					if (crc8 == data[rxBytes-1]) {
						ESP_LOGI(COMM_UART_TAG, "Valid command");
						//uint8_t data_len = data[1];	// currently not used

						if (data[2] == UART_COMM_MSG_CMD) {
							ESP_LOGD(COMM_UART_TAG, "Received command message");

							uart_comm_msg_t cmd_msg;
							cmd_msg.type = data[2];
							cmd_msg.id = (uint16_t)(data[3] << 8) + data[4];
							cmd_msg.cmd = data[5];
							for (i = 0; i < UART_COMM_MSG_PARAM_MAX_LEN; i++) {
								cmd_msg.params[i] = data[6 + i];
							}

							xQueueSend(uart_cmd_data_process_queue, ( void * )&cmd_msg, 1000/portTICK_PERIOD_MS);
						}
						else if (data[2] == UART_COMM_MSG_RESP) {
							ESP_LOGD(COMM_UART_TAG, "Received response message");
							// TODO:

							msg_id = (uint16_t)(data[3] << 8) + data[4];
							if ((msg_id == updateFwId) && (data[5] == UART_COMM_RESP_OK)) {
								// receive response update fw request ok then start to send firmware data
								uart_comm_msg_t fw_update_msg;
								fw_update_msg.type = UART_COMM_MSG_FW;

								// send
								uart_comm_send_msg(fw_update_msg);
							}
						}
						else if (data[2] == UART_COMM_MSG_STATE) {
							ESP_LOGD(COMM_UART_TAG, "Received state message");

							uart_state_msg_t state_msg;
							state_msg.type = data[2];
							// data[3], data[4] is msg_id, not used in state message
							state_msg.filter1_hour = (uint16_t)(data[5] << 8) + data[6];
							state_msg.filter2_hour = (uint16_t)(data[7] << 8) + data[8];
							state_msg.filter3_hour = (uint16_t)(data[9] << 8) + data[10];
							state_msg.filter4_hour = (uint16_t)(data[11] << 8) + data[12];
							state_msg.filter5_hour = (uint16_t)(data[13] << 8) + data[14];
							state_msg.tds_in = (uint16_t)(data[15] << 8) + data[16];
							state_msg.tds_out = (uint16_t)(data[17] << 8) + data[18];

							xQueueSend(uart_state_data_process_queue, ( void * )&state_msg, 1000/portTICK_PERIOD_MS);
						}
						else {
							ESP_LOGE(COMM_UART_TAG, "Invalid message type, got: %d", data[2]);
						}
					}
					else {
						ESP_LOGE(COMM_UART_TAG, "Invalid command crc8, got: %02x, calculated: %02x", data[rxBytes-1], crc8);
					}
				}
				else {
					ESP_LOGE(COMM_UART_TAG, "Invalid command header.");
				}
            }
            else {
				ESP_LOGE(COMM_UART_TAG, "Invalid command length.");
			}
        }
    }

    vTaskDelete(NULL);
}

esp_err_t uart_comm_send_msg(uart_comm_msg_t msg) {
	if (uart_cmd_data_send_queue != NULL) {
		if (xQueueSend(uart_cmd_data_send_queue, ( void * )&msg, 10/portTICK_PERIOD_MS) == pdPASS) {
			return ESP_OK;
		}
		else {
			return ESP_FAIL;
		}
	}
	else {
		return ESP_FAIL;
	}
}

static void uart_cmd_process_task(void *arg) {
	uart_comm_msg_t cmd_msg;
	uart_comm_msg_t resp_msg;

	for(;;) {
		vTaskDelay(10 / portTICK_PERIOD_MS);

		if (uart_cmd_data_process_queue != NULL) {
			if(xQueueReceive(uart_cmd_data_process_queue, &cmd_msg, portMAX_DELAY) == pdTRUE) {
				ESP_LOGD(COMM_UART_TAG, "Begin to process command data ...");
				//ESP_LOGD(COMM_UART_TAG, "Free memory %d", esp_get_free_heap_size());

				switch (cmd_msg.cmd)
				{
					case ON_BLE_CMD:
						resp_msg.type = UART_COMM_MSG_RESP;
						resp_msg.id = cmd_msg.id;
						resp_msg.status = UART_COMM_RESP_OK;

						// check if ble is disable then enable it
						if (app_config.is_ble_enable == BLE_DISABLE) {
							esp_err_t err = ble_reinit();
							if (err != ESP_OK) {
								ESP_LOGE(COMM_UART_TAG, "ble_reinit error");
								resp_msg.status = UART_COMM_RESP_ERROR;
							}
							else {
								// update app_config
								app_config.is_ble_enable = BLE_ENABLE;
								err = nvs_memory_set_app_config(&app_config);
								if (err != ESP_OK) {
									ESP_LOGE(COMM_UART_TAG, "Error! Fail to save app_config to nvs memory!\n");
								}
							}
						}

						// send response
						uart_comm_send_msg(resp_msg);
						ESP_LOGI(COMM_UART_TAG, "ON_BLE_CMD processed!");
						break;

					case DISCONNECT_DEVICE_CMD:
						// TODO:
						ESP_LOGI(COMM_UART_TAG, "DISCONNECT_DEVICE_CMD processed!");
						break;

					case RESET_DEFAULT_PASSWORD_CMD:
						// TODO:
						ESP_LOGI(COMM_UART_TAG, "RESET_DEFAULT_PASSWORD_CMD processed!");
						break;

					case RETURN_CURR_FW_VERSION_CMD:
						mcu_fw_version = cmd_msg.params[0];

						// update flag
						is_firmware_update_successfully = true;

						// notify to send sensor data including error immediately
						xEventGroupSetBits(system_event_group, GOT_MCU_FW_BIT);
						// notify fw version done
						xEventGroupSetBits(system_event_group, FW_UPDATE_DONE_BIT);

						break;

					case ERROR_CMD:
						// TODO:
						// check error bit to create ERR0000 string then send to server
						if (xSensorDataSemaphore != NULL) {
							if( xSemaphoreTake( xSensorDataSemaphore, 100/portTICK_PERIOD_MS ) == pdTRUE )
							{
								sensor_data.error = (uint32_t)((cmd_msg.params[0] << 16) + (cmd_msg.params[1] << 8) + cmd_msg.params[2]);
								sensor_data.error_changed = 1;

								xSemaphoreGive( xSensorDataSemaphore );

								if (isThingsboardConnEstablished() == ESP_OK) {
									// notify to send sensor data including error immediately
									xEventGroupSetBits(system_event_group, MACHINE_ERROR_EVENT);
								}
								else if (system_time_updated == true) {	// system time is updated from ntp or rtc
									mc_errors.error = (uint32_t)((cmd_msg.params[0] << 16) + (cmd_msg.params[1] << 8) + cmd_msg.params[2]);
									get_current_timestamp(mc_errors.time);
									mc_errors.error_sync = 0;

									// save to nvs
									if (nvs_memory_set_mc_errors(&mc_errors) != ESP_OK) {
										ESP_LOGE(COMM_UART_TAG, "nvs_memory_set_mc_errors: fail!");
									}
								}
								else {
									/* RTC is not configured then reject this error */
								}
							}
							else {
								resp_msg.status = UART_COMM_RESP_ERROR;
							}
						}

						// check if not connected server then save this error for later sending
						// timestamp
						// TODO:


						resp_msg.type = UART_COMM_MSG_RESP;
						resp_msg.id = cmd_msg.id;
						resp_msg.status = UART_COMM_RESP_OK;

						// send response
						uart_comm_send_msg(resp_msg);
						ESP_LOGI(COMM_UART_TAG, "ERROR_CMD processed!");
						break;


					default:
						ESP_LOGE(COMM_UART_TAG, "UNKNOWN uart comm command: %02x!", cmd_msg.cmd);
				}



			}
		}
	}

	vTaskDelete(NULL);
}

static void uart_state_process_task(void *arg) {
	uart_state_msg_t state_msg;

	for(;;) {
		vTaskDelay(10 / portTICK_PERIOD_MS);

		if (uart_state_data_process_queue != NULL) {
			if(xQueueReceive(uart_state_data_process_queue, &state_msg, portMAX_DELAY) == pdTRUE) {
				ESP_LOGD(COMM_UART_TAG, "Begin to process state data ...");
				//ESP_LOGI(COMM_UART_TAG, "Free memory %d", esp_get_free_heap_size());
				if (xSensorDataSemaphore != NULL) {
					if( xSemaphoreTake( xSensorDataSemaphore, 100/portTICK_PERIOD_MS ) == pdTRUE )
					{
						sensor_data.filter1_remain_time = state_msg.filter1_hour;
						sensor_data.filter2_remain_time = state_msg.filter2_hour;
						sensor_data.filter3_remain_time = state_msg.filter3_hour;
						sensor_data.filter4_remain_time = state_msg.filter4_hour;
						sensor_data.filter5_remain_time = state_msg.filter5_hour;
						sensor_data.purity_water_in = state_msg.tds_in;
						sensor_data.purity_water_out = state_msg.tds_out;

						xSemaphoreGive( xSensorDataSemaphore );
					}
					else {
						ESP_LOGE(COMM_UART_TAG, "ERROR! Cannot take xSensorDataSemaphore");
					}
				}
				else {
					ESP_LOGE(COMM_UART_TAG, "ERROR! Invalid xSensorDataSemaphore");
				}
			}
		}
	}

	vTaskDelete(NULL);
}

void uart_comm_init(void) {

	vTaskDelay(100 / portTICK_PERIOD_MS);
	// calculate crc16 of binary bin
	binaryBinCrc16 = crc_16(firmware_bin_start, firmware_bin_length);
	ESP_LOGI(COMM_UART_TAG, "Binary CRC16 = %04X", binaryBinCrc16);

	// create queue
	uart_cmd_data_send_queue = xQueueCreate(3, sizeof(uart_comm_msg_t));
	uart_cmd_data_process_queue = xQueueCreate(3, sizeof(uart_comm_msg_t));
	uart_state_data_process_queue = xQueueCreate(3, sizeof(uart_state_msg_t));

	// create semaphore
	xSensorDataSemaphore = xSemaphoreCreateMutex();

	/* Configure parameters of an UART driver,
	 * communication pins and install the driver */
	uart_config_t uart_config = {
		.baud_rate = COMM_UART_BAUD_RATE,
		.data_bits = UART_DATA_8_BITS,
		.parity    = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
		.source_clk = UART_SCLK_APB,
	};
	int intr_alloc_flags = 0;

	ESP_ERROR_CHECK(uart_driver_install(COMM_UART_PORT_NUM, COMM_UART_BUF_SIZE, 0, 0, NULL, intr_alloc_flags));
	ESP_ERROR_CHECK(uart_param_config(COMM_UART_PORT_NUM, &uart_config));
	ESP_ERROR_CHECK(uart_set_pin(COMM_UART_PORT_NUM, COMM_UART_TXD, COMM_UART_RXD, COMM_UART_RTS, COMM_UART_CTS));

	xTaskCreate(uart_comm_rx_task, "uart_comm_rx_task", 1024*3, NULL, 10, NULL);
	xTaskCreate(uart_comm_tx_task, "uart_comm_tx_task", 1024*3, NULL, 10, NULL);
	xTaskCreate(uart_cmd_process_task, "uart_cmd_process_task", 2*1024, NULL, 10, NULL);
	xTaskCreate(uart_state_process_task, "uart_state_process_task", 2*1024, NULL, 10, NULL);
}
