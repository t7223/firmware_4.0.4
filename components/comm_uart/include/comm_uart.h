#ifndef _COMM_UART_H_
#define _COMM_UART_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "common.h"
#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif


#define COMM_UART_RXD 26
#define COMM_UART_TXD 25

#define COMM_UART_RTS (UART_PIN_NO_CHANGE)
#define COMM_UART_CTS (UART_PIN_NO_CHANGE)

#define COMM_UART_PORT_NUM      		(1)
#define COMM_UART_BAUD_RATE     		(150000)
#define COMM_UART_TASK_STACK_SIZE    	(2048)

#define COMM_UART_BUF_SIZE (1024)

#define COMM_UART_MAX_MSG_LEN (1024)
#define COMM_UART_HEADER	0x7e


typedef enum {
	RESTART_DEVICE_CMD = 0x01,
	ON_BLE_CMD = 0x02,
	DISCONNECT_DEVICE_CMD = 0x03,
	RESET_DEFAULT_PASSWORD_CMD = 0x04,
	SYNC_TIME_CMD = 0x05,					// gui dinh ky tu esp32
	WIFI_CONNECTED_CMD = 0x06,
	SERVER_CONNECTED_CMD = 0x07,
	CONFIG_UPDATE_STATE_FREQ_CMD = 0x08,
	ERROR_CMD = 0x09,
	WIRELESS_OFF_STATE_CMD = 0x0A,	// ble off, no wifi connections
	RO_WASH_REQUEST_CMD = 0x0B,
	RO_WASH_START_CMD = 0x0C,
	RO_WASH_DONE_CMD = 0x0D,
	TOUCH_LOCK_REQUEST_CMD = 0x0E,
	TOUCH_LOCK_CMD = 0x0F,
	TOUCH_UNLOCK_REQUEST_CMD = 0x10,
	TOUCH_UNLOCK_CMD = 0x11,
	UPDATE_FW_REQUEST_CMD = 0x12,
	GET_CURR_FW_VERSION_CMD = 0x13,
	RETURN_CURR_FW_VERSION_CMD = 0x14,
	FW_UPDATE_STATUS_CMD = 0x15,
	BUZZER_ENABLE_REQUEST_CMD = 0x16,
	BUZZER_DISABLE_REQUEST_CMD = 0x17,

	RESET_FILTER1_COUNTER_CMD = 0x21,
	RESET_FILTER2_COUNTER_CMD = 0x22,
	RESET_FILTER3_COUNTER_CMD = 0x23,
	RESET_FILTER4_COUNTER_CMD = 0x24,
	RESET_FILTER5_COUNTER_CMD = 0x25,

} comm_command_t;




void uart_comm_init(void);
esp_err_t uart_comm_send_msg(uart_comm_msg_t msg);










#ifdef __cplusplus
}
#endif

#endif /* _COMM_UART_H_ */

