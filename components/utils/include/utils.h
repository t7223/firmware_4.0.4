#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINES
 ****************************************************************************************
 */
#define INTERNET_OK		1
#define INTERNET_FAIL	0

#define CRC8_POLY 		0x31
#define	CRC16_POLY		0xA001
#define	CRC16_START		0x0000


uint16_t crc_16(const uint8_t *data, size_t num_bytes);
uint16_t update_crc_16(uint16_t crc, uint8_t c);


uint8_t crc8_lsb(uint8_t poly, uint8_t* data, int size);
uint8_t crc8_msb(uint8_t poly, uint8_t* data, int size);


esp_err_t get_public_ipaddress(char *pubic_ip_str);
void ping_init_task(uint8_t *internet_status);
esp_err_t stop_ping_loop();



#ifdef __cplusplus
}
#endif

#endif /* _UTILS_H_ */
