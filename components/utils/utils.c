#include "utils.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "lwip/dns.h"
#include "lwip/inet.h"
#include "lwip/netdb.h"
#include "lwip/sockets.h"
#include "ping/ping_sock.h"

#include "esp_http_client.h"


#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 1024

#define UTILS_TAG	"UTILS"

uint8_t _internet_status = INTERNET_FAIL;
esp_ping_handle_t ping;


static void init_crc16_tab( void );

static bool	crc_tab16_init = false;
static uint16_t	crc_tab16[256];

esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
	static int output_len;       // Stores number of bytes read

    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(UTILS_TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(UTILS_TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(UTILS_TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
        	ESP_LOGD(UTILS_TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
        	ESP_LOGD(UTILS_TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);

        	/*
			 *  Check for chunked encoding is added as the URL for chunked encoding used in this example returns binary data.
			 *  However, event handler can also be used in case chunked encoding is used.
			 */
			if (!esp_http_client_is_chunked_response(evt->client) && (evt->data_len <= MAX_HTTP_OUTPUT_BUFFER)) {
				// If user_data buffer is configured, copy the response into the buffer
				if (evt->user_data) {
					memcpy(evt->user_data + output_len, evt->data, evt->data_len);
				}
			}
            break;
        case HTTP_EVENT_ON_FINISH:
        	ESP_LOGD(UTILS_TAG, "HTTP_EVENT_ON_FINISH");
            output_len = 0;
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGD(UTILS_TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}

esp_err_t get_public_ipaddress(char *pubic_ip_str) {
	esp_err_t ret = ESP_OK;

	char response_buff[MAX_HTTP_OUTPUT_BUFFER] = {0};

	esp_http_client_config_t config = {
	   .url = "http://api.ipify.org",
	   .event_handler = _http_event_handle,
	   .user_data = response_buff
	};
	esp_http_client_handle_t client = esp_http_client_init(&config);
	esp_http_client_set_method(client, HTTP_METHOD_GET);
	esp_err_t err = esp_http_client_perform(client);

	if (err != ESP_OK) {
		ESP_LOGE(UTILS_TAG, "Error (%s) esp_http_client_perform!\n", esp_err_to_name(err));
		ret = ESP_FAIL;
	} else {
		int status = esp_http_client_get_status_code(client);
		int content_length = esp_http_client_get_content_length(client);
		ESP_LOGD(UTILS_TAG, "Status = %d, content_length = %d\n", status, content_length);

	   if (status == 200) {
		   ESP_LOGI(UTILS_TAG, "Public IP address = %s\n", response_buff);
		   strcpy(pubic_ip_str, response_buff);
	   }
	}

	esp_http_client_close(client);
	esp_http_client_cleanup(client);

	return ret;
}

esp_err_t get_addr_info(char *addr, ip_addr_t *target_addr) {
	/* convert URL to IP address */
	struct addrinfo hint;
	struct addrinfo *res = NULL;
	if (getaddrinfo(addr, NULL, &hint, &res) != 0) {
		ESP_LOGE(UTILS_TAG, "ping: unknown host www.google.com\n");
		freeaddrinfo(res);
		return ESP_FAIL;
	}
	struct in_addr addr4 = ((struct sockaddr_in *) (res->ai_addr))->sin_addr;
	inet_addr_to_ip4addr(ip_2_ip4(target_addr), &addr4);
	freeaddrinfo(res);

	return ESP_OK;
}

esp_err_t get_google_addr_info(ip_addr_t *target_addr) {
	/* convert URL to IP address */
	// memset(target_addr, 0, sizeof(ip_addr_t));
	struct addrinfo hint = {
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};
	struct addrinfo *res = NULL;
	if (getaddrinfo("google.com", NULL, &hint, &res) != 0) {
		ESP_LOGE(UTILS_TAG, "ping: unknown host www.google.com\n");
		freeaddrinfo(res);
		return ESP_FAIL;
	}
	struct in_addr addr4 = ((struct sockaddr_in *) (res->ai_addr))->sin_addr;
	inet_addr_to_ip4addr(ip_2_ip4(target_addr), &addr4);
	freeaddrinfo(res);

	return ESP_OK;
}

static void _on_ping_success(esp_ping_handle_t hdl, void *args)
{
//    uint8_t ttl;
//    uint16_t seqno;
//    uint32_t elapsed_time, recv_len;
//    ip_addr_t target_addr;
//    esp_ping_get_profile(hdl, ESP_PING_PROF_SEQNO, &seqno, sizeof(seqno));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_TTL, &ttl, sizeof(ttl));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_IPADDR, &target_addr, sizeof(target_addr));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_SIZE, &recv_len, sizeof(recv_len));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_TIMEGAP, &elapsed_time, sizeof(elapsed_time));
//    printf("%d bytes from %s icmp_seq=%d ttl=%d time=%d ms\n",
//           recv_len, inet_ntoa(target_addr.u_addr.ip4), seqno, ttl, elapsed_time);

	//ESP_LOGD(UTILS_TAG, "INTERNET_OK\n");
	_internet_status = INTERNET_OK;

}

static void _on_ping_timeout(esp_ping_handle_t hdl, void *args)
{
//    uint16_t seqno;
//    ip_addr_t target_addr;
//    esp_ping_get_profile(hdl, ESP_PING_PROF_SEQNO, &seqno, sizeof(seqno));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_IPADDR, &target_addr, sizeof(target_addr));
//    printf("From %s icmp_seq=%d timeout\n", inet_ntoa(target_addr.u_addr.ip4), seqno);

	//ESP_LOGD(UTILS_TAG, "INTERNET_FAIL\n");
	_internet_status = INTERNET_FAIL;
}

static void _on_ping_end(esp_ping_handle_t hdl, void *args)
{
//    uint32_t transmitted;
//    uint32_t received;
//    uint32_t total_time_ms;
//
//    esp_ping_get_profile(hdl, ESP_PING_PROF_REQUEST, &transmitted, sizeof(transmitted));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_REPLY, &received, sizeof(received));
//    esp_ping_get_profile(hdl, ESP_PING_PROF_DURATION, &total_time_ms, sizeof(total_time_ms));
//    printf("%d packets transmitted, %d received, time %dms\n", transmitted, received, total_time_ms);

	//ESP_LOGD(UTILS_TAG, "INTERNET_FAIL\n");
	_internet_status = INTERNET_FAIL;
}

void start_ping_loop(ip_addr_t target_addr) {
	esp_ping_config_t ping_config = ESP_PING_DEFAULT_CONFIG();
	ping_config.target_addr = target_addr;          // target IP address
	ping_config.count = ESP_PING_COUNT_INFINITE;    // ping in infinite mode, esp_ping_stop can stop it

	/* set callback functions */
	esp_ping_callbacks_t cbs = {
		.on_ping_success = _on_ping_success,
		.on_ping_timeout = _on_ping_timeout,
		.on_ping_end = _on_ping_end,
		.cb_args = NULL
	};

	esp_ping_new_session(&ping_config, &cbs, &ping);
	esp_ping_start(ping);
}

static void ping_loop_task(void *param) {
	ESP_LOGI(UTILS_TAG, "Start ping_loop_task\n");

	esp_err_t err;
	ip_addr_t target_addr;

	do {
		err = get_google_addr_info(&target_addr);
		vTaskDelay(5000 / portTICK_PERIOD_MS);
		ESP_LOGD(UTILS_TAG, "Unable to get addr info of wwww.google.com \n" );
	}
	while (err != ESP_OK);

	// start ping loop with target_addr
	start_ping_loop(target_addr);

	vTaskDelete(NULL);
}



void ping_init_task(uint8_t *internet_status) {
	// init at default fail status
	*internet_status = INTERNET_FAIL;
	internet_status = &_internet_status;

	// create new task for init ping loop
	xTaskCreate(ping_loop_task, "ping_loop_task", 3*1024, NULL, 10, NULL);
}

esp_err_t stop_ping_loop() {
	esp_err_t err = esp_ping_stop(ping);
	if (err != ESP_OK) {
		ESP_LOGE(UTILS_TAG, "Error (%s) esp_ping_stop!\n", esp_err_to_name(err));
		return ESP_FAIL;
	}

	return ESP_OK;
}

uint8_t crc8_lsb(uint8_t poly, uint8_t* data, int size) {
	uint8_t crc = 0x00;
	int bit;

	while (size--) {
		crc ^= *data++;
		for (bit = 0; bit < 8; bit++) {
			if (crc & 0x01) {
				crc = (crc >> 1) ^ poly;
			} else {
				crc >>= 1;
			}
		}
	}

	return crc;
}

uint8_t crc8_msb(uint8_t poly, uint8_t* data, int size) {
	uint8_t crc = 0x00;
	int bit;

	while (size--) {
		crc ^= *data++;
		for (bit = 0; bit < 8; bit++) {
			if (crc & 0x80) {
				crc = (crc << 1) ^ poly;
			} else {
				crc <<= 1;
			}
		}
	}

	return crc;
}

/*
 * uint16_t crc_16( const uint8_t *data, size_t num_bytes);
 *
 * The function crc_16() calculates the 16 bits CRC16 in one pass for a byte
 * string of which the beginning has been passed to the function. The number of
 * bytes to check is also a parameter. The number of the bytes in the string is
 * limited by the constant SIZE_MAX.
 */

uint16_t crc_16( const uint8_t *data, size_t num_bytes ) {

	uint16_t crc;
	const uint8_t *ptr;
	size_t a;

	if ( ! crc_tab16_init ) init_crc16_tab();

	crc = CRC16_START;
	ptr = data;

	if ( ptr != NULL ) for (a=0; a<num_bytes; a++) {

		crc = (crc >> 8) ^ crc_tab16[ (crc ^ (uint16_t) *ptr++) & 0x00FF ];
	}

	return crc;

}

/*
 * uint16_t update_crc_16( uint16_t crc, uint8_t c );
 *
 * The function update_crc_16() calculates a new CRC-16 value based on the
 * previous value of the CRC and the next byte of data to be checked.
 */

uint16_t update_crc_16( uint16_t crc, uint8_t c ) {

	if ( ! crc_tab16_init ) init_crc16_tab();

	return (crc >> 8) ^ crc_tab16[ (crc ^ (uint16_t) c) & 0x00FF ];

}

/*
 * static void init_crc16_tab( void );
 *
 * For optimal performance uses the CRC16 routine a lookup table with values
 * that can be used directly in the XOR arithmetic in the algorithm. This
 * lookup table is calculated by the init_crc16_tab() routine, the first time
 * the CRC function is called.
 */

static void init_crc16_tab( void ) {

	uint16_t i;
	uint16_t j;
	uint16_t crc;
	uint16_t c;

	for (i=0; i<256; i++) {

		crc = 0;
		c   = i;

		for (j=0; j<8; j++) {

			if ( (crc ^ c) & 0x0001 ) crc = ( crc >> 1 ) ^ CRC16_POLY;
			else                      crc =   crc >> 1;

			c = c >> 1;
		}

		crc_tab16[i] = crc;
	}

	crc_tab16_init = true;

}

