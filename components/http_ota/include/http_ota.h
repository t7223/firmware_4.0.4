#ifndef _HTTP_OTA_
#define _HTTP_OTA_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

#include "cJSON.h"

#ifdef __cplusplus
extern "C" {
#endif


#define OTA_RECV_TIMEOUT 10000




esp_err_t get_current_firmware_version(char *fwVersion);
void http_ota_init(void);



#ifdef __cplusplus
}
#endif

#endif /* _HTTP_OTA_ */
