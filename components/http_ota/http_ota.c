#include "http_ota.h"
#include "esp_rom_md5.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_ota_ops.h"
#include "esp_log.h"
#include "common.h"
#include "thingsboard.h"
#include "nvs_memory.h"


#define TAG "HTTP_OTA_TAG"

#define CONFIG_SKIP_VERSION_CHECK


extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");
extern const uint8_t server_cert_pem_end[] asm("_binary_ca_cert_pem_end");

// compute md5 of download file
md5_context_t md5_context;
uint8_t digest[16];		// calculated digest
char calChksum[FW_CKSUM_MAX_LEN];

QueueHandle_t ota_msg_queue = NULL;

static esp_err_t validate_image_header(esp_app_desc_t *new_app_info)
{
    if (new_app_info == NULL) {
        return ESP_ERR_INVALID_ARG;
    }

    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_app_desc_t running_app_info;
    if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK) {
        ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
    }

#ifndef CONFIG_SKIP_VERSION_CHECK
    if (memcmp(new_app_info->version, running_app_info.version, sizeof(new_app_info->version)) == 0) {
        ESP_LOGW(TAG, "Current running version is the same as a new. We will not continue the update.");
        return ESP_FAIL;
    }
#endif

#ifdef CONFIG_BOOTLOADER_APP_ANTI_ROLLBACK
    /**
     * Secure version check from firmware image header prevents subsequent download and flash write of
     * entire firmware image. However this is optional because it is also taken care in API
     * esp_https_ota_finish at the end of OTA update procedure.
     */
    const uint32_t hw_sec_version = esp_efuse_read_secure_version();
    if (new_app_info->secure_version < hw_sec_version) {
        ESP_LOGW(TAG, "New firmware security version is less than eFuse programmed, %d < %d", new_app_info->secure_version, hw_sec_version);
        return ESP_FAIL;
    }
#endif

    return ESP_OK;
}

static esp_err_t _http_client_init_cb(esp_http_client_handle_t http_client)
{
    esp_err_t err = ESP_OK;
    /* Uncomment to add custom headers to HTTP request */
    // err = esp_http_client_set_header(http_client, "Custom-Header", "Value");
    return err;
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        //ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
    	//ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
    	//ESP_LOG_BUFFER_HEXDUMP(TAG, evt->data, evt->data_len+1, ESP_LOG_INFO);
    	esp_rom_md5_update(&md5_context, evt->data, evt->data_len);

        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}

void http_ota_task(void *pvParameter)
{
	vTaskDelay(100 / portTICK_PERIOD_MS);
    ESP_LOGI(TAG, "Starting HTTP OTA task");

    ota_msg_t msg;
    uint8_t i;
    esp_err_t err;

    while (1) {
    	if (ota_msg_queue != NULL) {
			if(xQueueReceive(ota_msg_queue, &msg, portMAX_DELAY) == pdTRUE) {
				ESP_LOGI(TAG, "FW Url: %s\n", msg.fwUrl);
				ESP_LOGI(TAG, "FW Version: %s\n", msg.fwVersion);
				ESP_LOGI(TAG, "FW cksum: %s\n", msg.fwCksum);

				// update attribute
				cJSON *root = cJSON_CreateObject();
				cJSON_AddTrueToObject(root, "isUpdateFw");

				char *root_string = cJSON_PrintUnformatted(root);
				err = thingsboard_publish_attribute(root_string);
				if (err != ESP_OK) {
					ESP_LOGE(TAG, "thingsboard_publish_attribute for isUpdateFw error!");
				}

				// update attribute nvs
				app_attr.is_update_fw = 1;
				if (nvs_memory_set_attributes(&app_attr) != ESP_OK) {
					ESP_LOGE(TAG, "nvs_memory_set_attributes: app_attr fail!");
				}
				vTaskDelay(100 / portTICK_PERIOD_MS);
				cJSON_Delete(root);

				// init md5
				esp_rom_md5_init(&md5_context);

				// begin to execute firmware update process
			    esp_err_t ota_finish_err = ESP_OK;
			    esp_http_client_config_t config = {
			        .url = msg.fwUrl,
			        .cert_pem = (char *)server_cert_pem_start,
			        .timeout_ms = OTA_RECV_TIMEOUT,
					.event_handler = _http_event_handler,
			        .keep_alive_enable = true,
			    };


			    // skip common name check
			    config.skip_cert_common_name_check = true;

			    esp_https_ota_config_t ota_config = {
			        .http_config = &config,
			        .http_client_init_cb = _http_client_init_cb, // Register a callback to be invoked after esp_http_client is initialized
			    };

			    esp_https_ota_handle_t https_ota_handle = NULL;
			    esp_err_t err = esp_https_ota_begin(&ota_config, &https_ota_handle);
			    if (err != ESP_OK) {
			        ESP_LOGE(TAG, "ESP HTTPS OTA Begin failed");
			        vTaskDelay(100 / portTICK_PERIOD_MS);
			        continue;
			    }

			    esp_app_desc_t app_desc;
			    err = esp_https_ota_get_img_desc(https_ota_handle, &app_desc);
			    if (err != ESP_OK) {
			        ESP_LOGE(TAG, "esp_https_ota_read_img_desc failed");
			        goto ota_end;
			    }
			    err = validate_image_header(&app_desc);
			    if (err != ESP_OK) {
			        ESP_LOGE(TAG, "image header verification failed");
			        goto ota_end;
			    }

			    while (1) {
			        err = esp_https_ota_perform(https_ota_handle);
			        if (err != ESP_ERR_HTTPS_OTA_IN_PROGRESS) {
			            break;
			        }
			        // esp_https_ota_perform returns after every read operation which gives user the ability to
			        // monitor the status of OTA upgrade by calling esp_https_ota_get_image_len_read, which gives length of image
			        // data read so far.
			        ESP_LOGD(TAG, "Image bytes read: %d", esp_https_ota_get_image_len_read(https_ota_handle));
			    }

			    esp_rom_md5_final(digest, &md5_context);
			    // ESP_LOG_BUFFER_HEXDUMP(TAG, digest, 16, ESP_LOG_INFO);


			    if (esp_https_ota_is_complete_data_received(https_ota_handle) != true) {
			        // the OTA image was not completely received and user can customise the response to this situation.
			        ESP_LOGE(TAG, "Complete data was not received.");
			    } else {
			    	// verify checksum
			    	if (strlen(msg.fwCksum) != 32) {
			    		ESP_LOGE(TAG, "Invalid checksum length: %d", strlen(msg.fwCksum));
			    		goto ota_end;
			    	}
			    	else {
			    		// get calChksum string
			    		memset(calChksum, 0, FW_CKSUM_MAX_LEN);
			    		sprintf(calChksum, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			    				digest[0], digest[1], digest[2], digest[3], digest[4], digest[5], digest[6], digest[7], digest[8], digest[9], digest[10], digest[11], digest[12], digest[13], digest[14], digest[15]);

			    		// convert got chksum to lowercase
			    		for(i = 0; i <= strlen(msg.fwCksum); i++){
			    			if((msg.fwCksum[i] >= 65) && (msg.fwCksum[i] <= 90))
			    				msg.fwCksum[i]=msg.fwCksum[i] + 32;
			    		}

			    		if (strcmp(msg.fwCksum, calChksum) != 0) {
			    			ESP_LOGE(TAG, "Invalid checksum, \nGot: %s \nCal: %s", msg.fwCksum, calChksum);
			    			goto ota_end;
			    		}
			    	}

			        ota_finish_err = esp_https_ota_finish(https_ota_handle);
			        if ((err == ESP_OK) && (ota_finish_err == ESP_OK)) {
			        	// update nvs flag to update firmware for renesas after rebooting
						mcu_fw_status.mcufw_need_update = 1;
						if (nvs_memory_set_mcu_fw_status(&mcu_fw_status) != ESP_OK) {
							ESP_LOGE(TAG, "nvs_memory_set_attributes: mcu_fw_status fail!");
						}
						vTaskDelay(100 / portTICK_PERIOD_MS);

			            ESP_LOGI(TAG, "ESP_HTTPS_OTA upgrade successful. Rebooting ...");
			            vTaskDelay(1000 / portTICK_PERIOD_MS);
			            esp_restart();
			        } else {
			            if (ota_finish_err == ESP_ERR_OTA_VALIDATE_FAILED) {
			                ESP_LOGE(TAG, "Image validation failed, image is corrupted");
			            }
			            ESP_LOGE(TAG, "ESP_HTTPS_OTA upgrade failed 0x%x", ota_finish_err);
			            vTaskDelay(100 / portTICK_PERIOD_MS);

			            // update attribute
						cJSON *root1 = cJSON_CreateObject();
						cJSON_AddFalseToObject(root1, "isUpdateFw");

						char *root1_string = cJSON_PrintUnformatted(root1);
						err = thingsboard_publish_attribute(root1_string);
						if (err != ESP_OK) {
							ESP_LOGE(TAG, "thingsboard_publish_attribute for isUpdateFw error!");
						}

						// update attribute nvs
						app_attr.is_update_fw = 0;
						if (nvs_memory_set_attributes(&app_attr) != ESP_OK) {
							ESP_LOGE(TAG, "nvs_memory_set_attributes: app_attr fail!");
						}
						vTaskDelay(100 / portTICK_PERIOD_MS);
						cJSON_Delete(root1);

			            continue;
			        }
			    }

			ota_end:
			    esp_https_ota_abort(https_ota_handle);
			    ESP_LOGE(TAG, "ESP_HTTPS_OTA upgrade failed");

			    // update attribute
				cJSON *root2 = cJSON_CreateObject();
				cJSON_AddFalseToObject(root2, "isUpdateFw");

				char *root2_string = cJSON_PrintUnformatted(root2);
				err = thingsboard_publish_attribute(root2_string);
				if (err != ESP_OK) {
					ESP_LOGE(TAG, "thingsboard_publish_attribute for isUpdateFw error!");
				}

				// update attribute nvs
				app_attr.is_update_fw = 0;
				if (nvs_memory_set_attributes(&app_attr) != ESP_OK) {
					ESP_LOGE(TAG, "nvs_memory_set_attributes: app_attr fail!");
				}
				vTaskDelay(100 / portTICK_PERIOD_MS);
				cJSON_Delete(root2);

			    continue;
			}
    	}

    	vTaskDelay(100 / portTICK_PERIOD_MS);
    }

    vTaskDelete(NULL);
}

esp_err_t get_current_firmware_version(char *fwVersion) {
	const esp_partition_t *running = esp_ota_get_running_partition();
	esp_app_desc_t running_app_info;
	if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK) {
		ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
		strcpy(fwVersion, running_app_info.version);
		return ESP_OK;
	}
	else return ESP_FAIL;
}



void http_ota_init(void) {

	ota_msg_queue = xQueueCreate(2, sizeof(ota_msg_t));

#if defined(CONFIG_BOOTLOADER_APP_ROLLBACK_ENABLE) && defined(CONFIG_BOOTLOADER_APP_ANTI_ROLLBACK)
    /**
     * We are treating successful WiFi connection as a checkpoint to cancel rollback
     * process and mark newly updated firmware image as active. For production cases,
     * please tune the checkpoint behavior per end application requirement.
     */
    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_ota_img_states_t ota_state;
    if (esp_ota_get_state_partition(running, &ota_state) == ESP_OK) {
        if (ota_state == ESP_OTA_IMG_PENDING_VERIFY) {
            if (esp_ota_mark_app_valid_cancel_rollback() == ESP_OK) {
                ESP_LOGI(TAG, "App is valid, rollback cancelled successfully");
            } else {
                ESP_LOGE(TAG, "Failed to cancel rollback");
            }
        }
    }
#endif

    xTaskCreate(&http_ota_task, "http_ota_task", 1024 * 5, NULL, 5, NULL);
}






