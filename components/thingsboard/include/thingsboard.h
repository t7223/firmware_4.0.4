#ifndef _THINGSBOARD_H_
#define _THINGSBOARD_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MQTT_TOPIC_LEN		256
#define MQTT_MSG_DATA_LEN	1024
#define MQTT_CMD_LEN		256

/**
 * @brief   Thingsboard status
 */
typedef enum {
    TB_PROVISION_REQUEST,
	TB_PROVISION_CONNECTED,
    TB_TOKEN_AUTHEN_REQUEST,
	TB_TOKEN_AUTHEN_CONNECTED,
	TB_DISCONNECTED
} thingsboard_state_t;

/**
 * @brief   server conn status
 */
typedef enum {
    SERVER_CONNECTED,
	SERVER_DISCONNECTED
} sv_conn_status_t;


typedef struct {
	uint32_t request_id;
	uint8_t data[MQTT_MSG_DATA_LEN];
} __attribute__((packed))  MQTT_Message_t;











extern sv_conn_status_t sv_conn_status;
extern char secret_key[];
extern QueueHandle_t ota_msg_queue;


void thingsboard_init(void);
esp_err_t thingsboard_provision_request(void);
esp_err_t thingsboard_claim_request(void);
esp_err_t thingsboard_set_default_value(void);
esp_err_t thingsboard_publish_telemetry(char *data);
esp_err_t isThingsboardConnEstablished (void);
esp_err_t thingsboard_publish_attribute(char *data);

#ifdef __cplusplus
}
#endif

#endif /* _THINGSBOARD_H_ */
