#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "mqtt_client.h"

#include "common.h"
#include "thingsboard.h"
#include "ble.h"
#include "nvs_memory.h"
#include "wifi.h"
#include "comm_uart.h"


#define SECRET_KEY_LEN			16
#define REQUEST_DATA_MAX_LEN	1024
#define THINGSBOARD_TAG  "THINGSBOARD"

thingsboard_state_t tb_state = TB_DISCONNECTED;
sv_conn_status_t sv_conn_status = SERVER_DISCONNECTED;

char secret_key[SECRET_KEY_LEN + 2];
esp_mqtt_client_handle_t _client;

QueueHandle_t mqtt_data_queue = NULL;
static char mqtt_topic[MQTT_TOPIC_LEN];

SemaphoreHandle_t xPublishSemaphore = NULL;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;

    // your_context_t *context = event->context;
    switch (event->event_id) {
		case MQTT_EVENT_ANY:
			ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_ANY");
			break;
    	case MQTT_EVENT_BEFORE_CONNECT:
    		ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_BEFORE_CONNECT");
    		break;
    	case MQTT_EVENT_DELETED:
			ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_DELETED");
			break;
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_CONNECTED");
            sv_conn_status = SERVER_CONNECTED;

            if (tb_state == TB_PROVISION_REQUEST) {
            	msg_id = esp_mqtt_client_subscribe(client, MQTT_SUB_PROVISION_DEDAUT, 0);

            	if (msg_id < 0) {
            		ESP_LOGE(THINGSBOARD_TAG, "ERROR! Fail to subcribe %s, msg_id= %d\n", MQTT_SUB_PROVISION_DEDAUT, msg_id);
            		xEventGroupSetBits(system_event_group, PROV_RESTART_EVENT);
            		break;
            	}
            	tb_state = TB_PROVISION_CONNECTED;
            	xEventGroupSetBits(system_event_group, PROV_CONNECTED_EVENT);
            }
            else if (tb_state == TB_TOKEN_AUTHEN_REQUEST) {
            	msg_id = esp_mqtt_client_subscribe(client, MQTT_SUB_CMD_TOPIC_DEFAULT, 0);
            	if (msg_id < 0) {
					ESP_LOGE(THINGSBOARD_TAG, "ERROR! Fail to subcribe %s, msg_id= %d\n", MQTT_SUB_CMD_TOPIC_DEFAULT, msg_id);
					xEventGroupSetBits(system_event_group, PROV_RESTART_EVENT);
					break;
				}

            	xEventGroupSetBits(system_event_group, PROV_AUTHEN_SUCCESS_EVENT);
            }
            else {
            	/* not occur */
            }

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_DISCONNECTED");
            sv_conn_status = SERVER_DISCONNECTED;
            tb_state = TB_DISCONNECTED;

            // reset provision loop
            xEventGroupSetBits(system_event_group, PROV_RESTART_EVENT);

            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            //ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_DATA");
            //ESP_LOGI(THINGSBOARD_TAG, "TOPIC = %s\n", event->topic);
            //ESP_LOGI(THINGSBOARD_TAG, "DATA = %s\n", event->data);
        	memset(mqtt_topic, 0, MQTT_TOPIC_LEN);
        	memcpy(mqtt_topic, event->topic, event->topic_len);

            if (strcmp(event->topic, MQTT_SUB_PROVISION_DEDAUT) == 0) {
            	cJSON *prov_request_root = cJSON_Parse((char*)event->data);
            	if (cJSON_GetObjectItem(prov_request_root, "credentialsValue") && cJSON_GetObjectItem(prov_request_root, "credentialsType") && cJSON_GetObjectItem(prov_request_root, "status")) {
            		char *token = cJSON_GetObjectItem(prov_request_root, "credentialsValue")->valuestring;
            		char *type = cJSON_GetObjectItem(prov_request_root, "credentialsType")->valuestring;
            		char *status = cJSON_GetObjectItem(prov_request_root, "status")->valuestring;

            		if ((strcmp(type, "ACCESS_TOKEN") == 0) && (strcmp(status, "SUCCESS") == 0)) {
            			strcpy(app_config.device_token, token);
            			app_config.is_provisioned = DEVICE_PROVISIONED;

            			// save app_config
            			esp_err_t err = nvs_memory_set_app_config(&app_config);
            			if (err != ESP_OK) {
							ESP_LOGE(THINGSBOARD_TAG, "Error! Fail to save app_config to nvs memory!\n");
						}

            			// send provision request success
            			xEventGroupSetBits(system_event_group, PROV_REQUEST_SUCCESS_EVENT);
            		}
            		else {
            			// invalid response, need to request provision again
						// send provision request fail due to invalid response
						xEventGroupSetBits(system_event_group, PROV_REQUEST_FAIL_EVENT);
            		}
            	}
            	else {
            		// invalid response, need to request provision again
            		// send provision request fail due to invalid response
            		xEventGroupSetBits(system_event_group, PROV_REQUEST_FAIL_EVENT);
            	}
            	cJSON_Delete(prov_request_root);
            }
            else if (strstr(mqtt_topic, MQTT_SUB_CMD_PRE_DEFAULT) != NULL) {
            	// copy string with exact length then send to queue for further processing
            	MQTT_Message_t msg;
				int request_id;
				sscanf( mqtt_topic, "v1/devices/me/rpc/request/%d", &request_id );
				ESP_LOGD(THINGSBOARD_TAG, "request_id=%d", request_id);
				ESP_LOGD(THINGSBOARD_TAG, "data_len=%d", event->data_len);
				memset(msg.data, 0x0, MQTT_MSG_DATA_LEN);
				memcpy(msg.data, event->data, event->data_len);
				msg.request_id = request_id;
				xQueueSend(mqtt_data_queue, ( void * )&msg, 1000/portTICK_PERIOD_MS);
            }
            else {
            	/* Unhandled case */
            }

            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(THINGSBOARD_TAG, "MQTT_EVENT_ERROR");
            break;
    }

    return ESP_OK;
}






static void thingsboard_task(void *param) {
	ESP_LOGI(THINGSBOARD_TAG, "Start thingsboard_task\n");
	EventBits_t uxBits;

	/* Waiting until the connection is established (WIFI_CONNECTED_BIT) */
	xEventGroupWaitBits(system_event_group, WIFI_CONNECTED_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
	ESP_LOGI(THINGSBOARD_TAG, "Wifi is connected. Start provision process ...\n");

	while (1) {
		if (wifi_info->wifi_state != WIFI_STA_CONNECTED) {
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			continue;
		}

		// check for provisioned or not
		if (app_config.is_provisioned == DEVICE_PROVISIONED) {
			ESP_LOGD(THINGSBOARD_TAG, "Token= %s\n", app_config.device_token);
			tb_state = TB_TOKEN_AUTHEN_REQUEST;

			const esp_mqtt_client_config_t mqtt_provision_authen_cfg = {
				.uri = MQTT_BROKER_URI,
				.username = app_config.device_token,
				.event_handle = mqtt_event_handler,
			};

			if (_client != NULL) {
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				_client = NULL;
			}

			_client = esp_mqtt_client_init(&mqtt_provision_authen_cfg);
			if (_client == NULL) {
				// error then restart
				esp_restart();
				vTaskDelay(10000 / portTICK_PERIOD_MS);
			}
			if (esp_mqtt_client_start(_client) != ESP_OK) {
				// error then restart
				esp_restart();
				vTaskDelay(10000 / portTICK_PERIOD_MS);
			}

			// wait for response event in 60 seconds
			uxBits = xEventGroupWaitBits(system_event_group, PROV_AUTHEN_SUCCESS_EVENT | PROV_AUTHEN_FAIL_EVENT | PROV_RESTART_EVENT, pdTRUE, pdFALSE, 60000 / portTICK_PERIOD_MS);

			if ((uxBits & PROV_AUTHEN_SUCCESS_EVENT) != 0x00U) {
				ESP_LOGI(THINGSBOARD_TAG, "Provision authen successfully.\n");

				tb_state = TB_TOKEN_AUTHEN_CONNECTED;

				// notify renesas
				uart_comm_msg_t comm_msg;
				comm_msg.type = UART_COMM_MSG_CMD;
				comm_msg.id = (uint16_t) (esp_random() & 0xffff);
				comm_msg.cmd = SERVER_CONNECTED_CMD;

				uart_comm_send_msg(comm_msg);

				// request to send old data if having
				xEventGroupSetBits(system_event_group, SEND_OLD_DATA_REQUEST_BIT);

				// wait for provision restart event from ble
				xEventGroupWaitBits(system_event_group, PROV_RESTART_EVENT, pdTRUE, pdFALSE, portMAX_DELAY);
				ESP_LOGI(THINGSBOARD_TAG, "Provision restart ...\n");
				// unsubcribe MQTT_SUB_TOPIC_DEFAULT channel
				esp_mqtt_client_unsubscribe(_client, MQTT_SUB_CMD_TOPIC_DEFAULT);

				// stop current provision request client
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				_client = NULL;

				// notify renesas
				uart_comm_msg_t comm_msg1;
				comm_msg1.type = UART_COMM_MSG_CMD;
				comm_msg1.id = (uint16_t) (esp_random() & 0xffff);
				comm_msg1.cmd = WIFI_CONNECTED_CMD;

				uart_comm_send_msg(comm_msg1);

				continue;
			}
			else if ((uxBits & PROV_AUTHEN_FAIL_EVENT) != 0x00U) {
				ESP_LOGI(THINGSBOARD_TAG, "ERROR! Provision authen Fail, need to request provision authen again ...\n");

				// stop current provision request client
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				continue;
			}
			else if ((uxBits & PROV_RESTART_EVENT) != 0x00U) {
				ESP_LOGI(THINGSBOARD_TAG, "Provision restart ...\n");
				// unsubcribe MQTT_SUB_TOPIC_DEFAULT channel
				esp_mqtt_client_unsubscribe(_client, MQTT_SUB_CMD_TOPIC_DEFAULT);

				// stop current provision request client
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				_client = NULL;
				continue;
			}
			else {	// timeout
				ESP_LOGI(THINGSBOARD_TAG, "ERROR! Provision authen Timeout, need to request provision authen again ...\n");
				// stop current provision request client
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				_client = NULL;
				continue;
			}
		}
		else {	/* app_config.is_provisioned == DEVICE_NOT_PROVISIONED: device is not installed. It need to installed first */
			// start to send provision request
			tb_state = TB_PROVISION_REQUEST;

			const esp_mqtt_client_config_t mqtt_provision_request_cfg = {
				.uri = MQTT_BROKER_URI,
				.username = "provision",
				.event_handle = mqtt_event_handler,
			};

			if (_client != NULL) {
				esp_mqtt_client_destroy(_client);
//				esp_mqtt_client_disconnect(_client);
//				esp_mqtt_client_stop(_client);
				_client = NULL;
			}

			_client = esp_mqtt_client_init(&mqtt_provision_request_cfg);
			if (_client == NULL) {
				// error then restart
				esp_restart();
				vTaskDelay(10000 / portTICK_PERIOD_MS);
			}
			if (esp_mqtt_client_start(_client) != ESP_OK) {
				// error then restart
				esp_restart();
				vTaskDelay(10000 / portTICK_PERIOD_MS);
			}

			// waiting for device ready
			uxBits = xEventGroupWaitBits(system_event_group, PROV_RESTART_EVENT, pdTRUE, pdFALSE, portMAX_DELAY);
			ESP_LOGI(THINGSBOARD_TAG, "Provision restart ...\n");
			// unsubcribe MQTT_SUB_PROVISION_DEDAUT channel
			esp_mqtt_client_unsubscribe(_client, MQTT_SUB_PROVISION_DEDAUT);

			// stop current provision request client
			esp_mqtt_client_destroy(_client);
//			esp_mqtt_client_disconnect(_client);
//			esp_mqtt_client_stop(_client);
			_client = NULL;
		}

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

esp_err_t thingsboard_provision_request(void) {
	esp_err_t err;
	EventBits_t uxBits;
	int msg_id;
	int cnt = 0;
	uint8_t authen_connected = 0;
	char provisionRequest[REQUEST_DATA_MAX_LEN];

	// check to change to not provisioned state
	if (app_config.is_provisioned == DEVICE_PROVISIONED) {
		app_config.is_provisioned = DEVICE_NOT_PROVISIONED;

		// save app_config
		err = nvs_memory_set_app_config(&app_config);
		if (err != ESP_OK) {
			ESP_LOGE(THINGSBOARD_TAG, "Error! Fail to save app_config to nvs memory!\n");
		}

		// clear PROV_CONNECTED_EVENT event bit
		xEventGroupClearBits(system_event_group, PROV_CONNECTED_EVENT);
		// reset provision loop
		xEventGroupSetBits(system_event_group, PROV_RESTART_EVENT);

		// wait for TB_PROVISION connected
		uxBits = xEventGroupWaitBits(system_event_group, PROV_CONNECTED_EVENT, pdTRUE, pdFALSE, 15000 / portTICK_PERIOD_MS);

		if ((uxBits & PROV_CONNECTED_EVENT) == 0x00U) {	// timeout occur
			ESP_LOGE(THINGSBOARD_TAG, "Error! device is not in TB_PROVISION_CONNECTED state!\n");
			return ESP_FAIL;
		}
	}

	if (tb_state == TB_PROVISION_CONNECTED) {
		memset(provisionRequest, 0, REQUEST_DATA_MAX_LEN);
		provisionRequest[0] = '\0';
		sprintf(provisionRequest, "{\"provisionDeviceKey\":\"%s\", \"provisionDeviceSecret\":\"%s\", \"deviceName\":\"%s\"}", PROVISION_DEVICE_KEY, PROVISION_DEVICE_SECRET, app_config.device_id);

		// public to provision request channel
		msg_id = esp_mqtt_client_publish(_client, MQTT_PUB_PROVISION_DEDAUT, provisionRequest, 0, 0, 0);
		ESP_LOGI(THINGSBOARD_TAG, "sent publish, msg_id=%d", msg_id);

		if (msg_id < 0) {
			ESP_LOGE(THINGSBOARD_TAG, "ERROR! Fail to publish %s, msg_id= %d\n", MQTT_PUB_PROVISION_DEDAUT, msg_id);
			return ESP_FAIL;
		}

		// wait for response
		EventBits_t uxBits = xEventGroupWaitBits(system_event_group, PROV_REQUEST_SUCCESS_EVENT | PROV_REQUEST_FAIL_EVENT, pdTRUE, pdFALSE, 15000 / portTICK_PERIOD_MS);

		if ((uxBits & PROV_REQUEST_SUCCESS_EVENT) != 0x00U) {
			// break provisioning state, move to provisioned to loging using token
			xEventGroupSetBits(system_event_group, PROV_RESTART_EVENT);

			// wait for provision successfully in 15 seconds
			authen_connected = 0;
			for (cnt = 0; cnt < 15; cnt++) {
				if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
					authen_connected = 1;
					break;
				}

				vTaskDelay(1000 / portTICK_PERIOD_MS);
			}

			if (authen_connected != 0) {
				return ESP_OK;
			}
			else {
				return ESP_FAIL;
			}
		}
		else {
			return ESP_FAIL;
		}
	}
	else {
		ESP_LOGE(THINGSBOARD_TAG, "thingsboard_provision_request error! device is not in TB_PROVISION_CONNECTED state!\n");
		return ESP_FAIL;
	}
}

esp_err_t thingsboard_claim_request(void) {
	if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
		memset(secret_key, 0, SECRET_KEY_LEN + 2);
		uint32_t rand01 = esp_random();
		uint32_t rand02 = esp_random();
		sprintf(secret_key, "%08x%08x", rand01, rand02);
		ESP_LOGD(THINGSBOARD_TAG, "SecretKey: %s\n", secret_key);
		// create account information
		cJSON *register_root = cJSON_CreateObject();
		cJSON_AddStringToObject(register_root, "secretKey", secret_key);
		cJSON_AddNumberToObject(register_root, "durationMs", (double)60000);
		char *register_string = cJSON_PrintUnformatted(register_root);
		ESP_LOGI(THINGSBOARD_TAG, "register_string: %s\n", register_string);
		// publish to register channel
		int msg_id = esp_mqtt_client_publish(_client, MQTT_PUB_REGISTER_DEFAULT, register_string, 0, 0, 0);
		ESP_LOGI(THINGSBOARD_TAG, "sent publish, msg_id=%d", msg_id);
		cJSON_Delete(register_root);

		if (msg_id < 0) {
			ESP_LOGE(THINGSBOARD_TAG, "ERROR! Fail to publish %s, msg_id= %d\n", MQTT_PUB_REGISTER_DEFAULT, msg_id);
			return ESP_FAIL;
		}

		vTaskDelay(1000 / portTICK_PERIOD_MS);
		return ESP_OK;
	}
	else {
		ESP_LOGE(THINGSBOARD_TAG, "thingsboard_claim_request error! device is not in TB_PROVISION_CONNECTED state!\n");
		return ESP_FAIL;
	}
}

esp_err_t thingsboard_set_default_value(void) {
	char strTmp[16];
	int cnt = 0;

	for (cnt = 0; cnt < 25; cnt++) {
		if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
			break;
		}
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}

	ESP_LOGI("THINGSBOARD_TAG", "provision_time len: %d", strlen(provision_time));

	if ((strlen(provision_time) > 0) && (tb_state == TB_TOKEN_AUTHEN_CONNECTED)) {
		ESP_LOGI("THINGSBOARD_TAG", "Passsedddd");

		cJSON *root = cJSON_CreateObject();
		cJSON_AddStringToObject(root, "manufacturerTime", app_attr.manufacturer_time);
		cJSON_AddStringToObject(root, "provisionTime", provision_time);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.threshold);
		cJSON_AddStringToObject(root, "threshold", strTmp);
		cJSON_AddStringToObject(root, "fwVersion", app_attr.fw_version);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.filter1_total_time);
		cJSON_AddStringToObject(root, "totalTimeFilter1", strTmp);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.filter2_total_time);
		cJSON_AddStringToObject(root, "totalTimeFilter2", strTmp);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.filter3_total_time);
		cJSON_AddStringToObject(root, "totalTimeFilter3", strTmp);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.filter4_total_time);
		cJSON_AddStringToObject(root, "totalTimeFilter4", strTmp);
		memset(strTmp, 0, 16);
		sprintf(strTmp, "%u", app_attr.filter5_total_time);
		cJSON_AddStringToObject(root, "totalTimeFilter5", strTmp);
		if (app_attr.is_update_fw == 0) {
			cJSON_AddFalseToObject(root, "isUpdateFw");
		}
		else {
			cJSON_AddTrueToObject(root, "isUpdateFw");
		}

		char *root_string = cJSON_PrintUnformatted(root);

		int msg_id = esp_mqtt_client_publish(_client, MQTT_PUB_TOPIC_DEFVAL, root_string, 0, 0, 0);
		ESP_LOGI(THINGSBOARD_TAG, "sent publish, msg_id=%d", msg_id);
		vTaskDelay(100 / portTICK_PERIOD_MS);
		cJSON_Delete(root);

		if (msg_id < 0) {
			return ESP_FAIL;
		}
		else {
			return ESP_OK;
		}
	}
	else return ESP_FAIL;
}

esp_err_t isThingsboardConnEstablished (void) {
	if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
		return ESP_OK;
	}
	else return ESP_FAIL;
}

esp_err_t thingsboard_publish_telemetry(char *data) {
	int data_len = strlen(data);
	if (data_len > 0) {
		if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
			if (xPublishSemaphore != NULL) {
				if( xSemaphoreTake( xPublishSemaphore, 1000/portTICK_PERIOD_MS ) == pdTRUE )
				{
					int msg_id = esp_mqtt_client_publish(_client, MQTT_PUB_TELEMETRY_TOPIC_DEFAULT, data, 0, 0, 0);
					ESP_LOGI(THINGSBOARD_TAG, "sent publish, msg_id=%d", msg_id);

					xSemaphoreGive( xPublishSemaphore );

					if (msg_id < 0) {
						return ESP_FAIL;
					}
					else {
						return ESP_OK;
					}
				}
				else {
					return ESP_FAIL;
				}
			}
			else {
				return ESP_FAIL;
			}
		}
		else {
			return ESP_FAIL;
		}
	}
	else return ESP_FAIL;
}

esp_err_t thingsboard_publish_attribute(char *data) {
	int data_len = strlen(data);
	if (data_len > 0) {
		if (tb_state == TB_TOKEN_AUTHEN_CONNECTED) {
			if (xPublishSemaphore != NULL) {
				if( xSemaphoreTake( xPublishSemaphore, 1000/portTICK_PERIOD_MS ) == pdTRUE )
				{
					int msg_id = esp_mqtt_client_publish(_client, MQTT_PUB_TOPIC_DEFVAL, data, 0, 0, 0);
					ESP_LOGI(THINGSBOARD_TAG, "sent publish, msg_id=%d", msg_id);

					xSemaphoreGive( xPublishSemaphore );

					if (msg_id < 0) {
						return ESP_FAIL;
					}
					else {
						return ESP_OK;
					}
				}
				else {
					return ESP_FAIL;
				}
			}
			else {
				return ESP_FAIL;
			}
		}
		else {
			return ESP_FAIL;
		}
	}
	else return ESP_FAIL;
}

static void do_restart_task (void *param) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	// esp_restart();	// not used now, reset source is come from renesas ->  hardware reset

	// send reset message to renesas
	uart_comm_msg_t comm_msg;
	comm_msg.type = UART_COMM_MSG_CMD;
	comm_msg.id = (uint16_t) (esp_random() & 0xffff);
	comm_msg.cmd = RESTART_DEVICE_CMD;

	uart_comm_send_msg(comm_msg);

	vTaskDelay(500 / portTICK_PERIOD_MS);
	esp_restart();
	vTaskDelete(NULL);
}

static void thingsboard_mqtt_process_task(void *param) {
	MQTT_Message_t msg;
	char mqtt_pub_response[MQTT_TOPIC_LEN];
	int i;
	//esp_err_t err;
	EventBits_t uxBits;

	// ESP_LOGI(THINGSBOARD_TAG, "thingsboard_mqtt_data_process started!\n");

	for(;;) {
		vTaskDelay(10 / portTICK_PERIOD_MS);

		if (mqtt_data_queue != NULL) {
			if(xQueueReceive(mqtt_data_queue, &msg, portMAX_DELAY) == pdTRUE) {
				ESP_LOGD(THINGSBOARD_TAG, "Data Value: %s\n", msg.data);
				ESP_LOGD(THINGSBOARD_TAG, "Request id: %d\n", msg.request_id);
				cJSON *cmd_root = cJSON_Parse((char*)msg.data);
				cJSON *resp_root = cJSON_CreateObject();
				vTaskDelay(10 / portTICK_PERIOD_MS);

				if (cJSON_GetObjectItem(cmd_root, "method")) {
					char * command = cJSON_GetObjectItem(cmd_root, "method")->valuestring;

					if (strcmp(command, "onBle") == 0) {
						cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
						cJSON_AddStringToObject(resp_root, "cmd", command);
						cJSON_AddStringToObject(resp_root, "code", "MSG_0");

						// check if ble is disable then enable it
						if (app_config.is_ble_enable == BLE_DISABLE) {
							esp_err_t err = ble_reinit();
							if (err != ESP_OK) {
								ESP_LOGE(THINGSBOARD_TAG, "ble_reinit error");
							}
							else {
								// update app_config
								app_config.is_ble_enable = BLE_ENABLE;
								err = nvs_memory_set_app_config(&app_config);
								if (err != ESP_OK) {
									ESP_LOGE(THINGSBOARD_TAG, "Error! Fail to save app_config to nvs memory!\n");
								}
							}
						}
					}
					else if (strcmp(command, "restart") == 0) {
						cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
						cJSON_AddStringToObject(resp_root, "cmd", command);
						cJSON_AddStringToObject(resp_root, "code", "MSG_0");
						xTaskCreate(do_restart_task, "do_restart_task", 1024, NULL, 10, NULL);
					}
					else if (strcmp(command, "offBle") == 0) {
						cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
						cJSON_AddStringToObject(resp_root, "cmd", command);
						cJSON_AddStringToObject(resp_root, "code", "MSG_0");

						// check if ble is disable then enable it
						if (app_config.is_ble_enable == BLE_ENABLE) {
							esp_err_t err = ble_deinit();
							if (err != ESP_OK) {
								ESP_LOGE(THINGSBOARD_TAG, "ble_deinit error");
							}
							else {
								// update app_config
								app_config.is_ble_enable = BLE_DISABLE;
								err = nvs_memory_set_app_config(&app_config);
								if (err != ESP_OK) {
									ESP_LOGE(THINGSBOARD_TAG, "Error! Fail to save app_config to nvs memory!\n");
								}
							}
						}
					}
					else if (strcmp(command, "updateRTC") == 0) {
						cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
						cJSON_AddStringToObject(resp_root, "cmd", command);
						cJSON_AddStringToObject(resp_root, "code", "MSG_0");

						xEventGroupSetBits(system_event_group, REQUEST_RTC_UPDATE_BIT);
					}
					else if (strcmp(command, "getMCUFwVersion") == 0) {
						// send reset message to renesas
						uart_comm_msg_t comm_msg;
						comm_msg.type = UART_COMM_MSG_CMD;
						comm_msg.id = (uint16_t) (esp_random() & 0xffff);
						comm_msg.cmd = GET_CURR_FW_VERSION_CMD;

						xEventGroupClearBits(system_event_group, GOT_MCU_FW_BIT);
						uart_comm_send_msg(comm_msg);

						uxBits = xEventGroupWaitBits(system_event_group, GOT_MCU_FW_BIT, pdTRUE, pdFALSE, 1000 / portTICK_PERIOD_MS);

						if ((uxBits & GOT_MCU_FW_BIT) != 0x00U) {
							cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
							cJSON_AddStringToObject(resp_root, "cmd", command);
							cJSON_AddStringToObject(resp_root, "code", "MSG_0");
							cJSON_AddNumberToObject(resp_root, "version", mcu_fw_version);
						}
						else { // timeout
							cJSON_AddStringToObject(resp_root, "status", "0");		// chap nhan thuc  hien lenh
							cJSON_AddStringToObject(resp_root, "cmd", command);
							cJSON_AddStringToObject(resp_root, "code", "MSG_6");		// timeout
						}
					}
					else if (strcmp(command, "updateFW") == 0) {
						cJSON *params = cJSON_GetObjectItem(cmd_root, "params");
						if (params != NULL) {
							char *fwUrl = cJSON_GetObjectItem(params, "targetFwUrl")->valuestring;
							char *fwVersion = cJSON_GetObjectItem(params, "targetFwVer")->valuestring;
							char *fwCksum = cJSON_GetObjectItem(params, "cksum")->valuestring;

							// http://1.1.1.1
							if ((strlen(fwUrl) >= 14) && (strlen(fwVersion) >= 1) && (strlen(fwCksum) == 32)) {
								ota_msg_t otaMsg;
								memset(otaMsg.fwUrl, 0x0, FW_URL_MAX_LEN);
								strcpy(otaMsg.fwUrl, fwUrl);
								memset(otaMsg.fwVersion, 0x0, FW_VERSION_MAX_LEN);
								strcpy(otaMsg.fwVersion, fwVersion);
								memset(otaMsg.fwCksum, 0x0, FW_CKSUM_MAX_LEN);
								strcpy(otaMsg.fwCksum, fwCksum);
								xQueueSend(ota_msg_queue, ( void * )&otaMsg, 100/portTICK_PERIOD_MS);

								cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
								cJSON_AddStringToObject(resp_root, "cmd", command);
								cJSON_AddStringToObject(resp_root, "code", "MSG_0");
							}
							else {
								cJSON_AddStringToObject(resp_root, "status", "0");		// khong chap nhan thuc  hien lenh
								cJSON_AddStringToObject(resp_root, "cmd", command);
								cJSON_AddStringToObject(resp_root, "code", "MSG_5");
							}
						}
						else {
							cJSON_AddStringToObject(resp_root, "status", "0");		// khong chap nhan thuc  hien lenh
							cJSON_AddStringToObject(resp_root, "cmd", command);
							cJSON_AddStringToObject(resp_root, "code", "MSG_5");
						}
					}
					else if (strcmp(command, "reset") == 0) {
						cJSON *params = cJSON_GetObjectItem(cmd_root, "params");
						if (params != NULL) {
							cJSON *filterArr = cJSON_GetObjectItem(params, "filters");
							if (filterArr != NULL) {
								int arraySize = cJSON_GetArraySize(filterArr);

								if (arraySize > 0) {
									for (i = 0; i < arraySize; i++) {
										int filterId = cJSON_GetArrayItem(filterArr, i)->valueint;
										ESP_LOGI(THINGSBOARD_TAG, "filterId= %d", filterId);

										uart_comm_msg_t comm_msg;
										comm_msg.type = UART_COMM_MSG_CMD;
										comm_msg.id = (uint16_t) (esp_random() & 0xffff);
										comm_msg.cmd = RESET_FILTER1_COUNTER_CMD + filterId - 1;
										comm_msg.params[0] = 0;
										comm_msg.params[1] = 0;
										comm_msg.params[2] = 0;
										comm_msg.params[3] = 0;

										ESP_LOGI(THINGSBOARD_TAG, "comm_msg.cmd= %d", comm_msg.cmd);

										uart_comm_send_msg(comm_msg);
										vTaskDelay(50/portTICK_PERIOD_MS);
									}
								}

								cJSON_AddStringToObject(resp_root, "status", "1");		// chap nhan thuc  hien lenh
								cJSON_AddStringToObject(resp_root, "cmd", command);
								cJSON_AddStringToObject(resp_root, "code", "MSG_0");
							}
							else {
								cJSON_AddStringToObject(resp_root, "status", "0");		// khong chap nhan thuc  hien lenh
								cJSON_AddStringToObject(resp_root, "cmd", command);
								cJSON_AddStringToObject(resp_root, "code", "MSG_5");
							}
						}
						else {
							cJSON_AddStringToObject(resp_root, "status", "0");		// khong chap nhan thuc  hien lenh
							cJSON_AddStringToObject(resp_root, "cmd", command);
							cJSON_AddStringToObject(resp_root, "code", "MSG_5");
						}
					}
					else {
						cJSON_AddStringToObject(resp_root, "status", "0");		// khong chap nhan thuc  hien lenh
						cJSON_AddStringToObject(resp_root, "cmd", command);
						cJSON_AddStringToObject(resp_root, "code", "MSG_5");
					}
				}
				else {
					cJSON_AddStringToObject(resp_root, "status", "0");	// khong chap nhan thuc hien lenh
					cJSON_AddStringToObject(resp_root, "cmd", "unknown");
					cJSON_AddStringToObject(resp_root, "code", "MSG_5");
				}

				vTaskDelay(10 / portTICK_PERIOD_MS);
				char *resp_string = cJSON_PrintUnformatted(resp_root);
				ESP_LOGD(THINGSBOARD_TAG, "resp_string: %s\n", resp_string);

				// publish to response channel
				memset(mqtt_pub_response, 0, MQTT_TOPIC_LEN);
				sprintf(mqtt_pub_response, "%s%d", MQTT_PUB_CMDRESP_TOPIC_DEFAULT, msg.request_id);
				ESP_LOGD(THINGSBOARD_TAG, "mqtt_pub_response: %s\n", mqtt_pub_response);

				int msg_id = esp_mqtt_client_publish(_client, mqtt_pub_response, resp_string, 0, 0, 0);
				ESP_LOGD(THINGSBOARD_TAG, "sent command publish, msg_id=%d", msg_id);

				vTaskDelay(500 / portTICK_PERIOD_MS);
				cJSON_Delete(resp_root);
				cJSON_Delete(cmd_root);
			}
		}
	}

	vTaskDelete(NULL);
}



void thingsboard_init(void) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);

	// init variables
	tb_state = TB_DISCONNECTED;
	vTaskDelay(100 / portTICK_PERIOD_MS);

	// create queue
	mqtt_data_queue = xQueueCreate(3, sizeof(MQTT_Message_t));

	// init semaphore
	xPublishSemaphore = xSemaphoreCreateMutex();


	// create new task for init thingsboard
	xTaskCreate(thingsboard_task, "thingsboard_task", 5*1024, NULL, 10, NULL);

	// create new task for init thingsboard
	xTaskCreate(thingsboard_mqtt_process_task, "thingsboard_mqtt_process_task", 5*1024, NULL, 10, NULL);
}





