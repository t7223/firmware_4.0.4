#ifndef _BLE_H_
#define _BLE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * DEFINES
 ****************************************************************************************
 */

#define MAX_DEVICE_NAME_LEN		(40)
#define DATA_MAX_LEN           	(512)
#define MSG_DATA_LEN			(100)
#define CMD_MAX_LEN            	(20)
#define STATUS_MAX_LEN         	(20)

///Attributes State Machine
enum {
    IDX_SVC,

    IDX_DATA_RECV_CHAR,
    IDX_DATA_RECV_VAL,

    IDX_DATA_NOTIFY_CHAR,
    IDX_DATA_NTY_VAL,
    IDX_DATA_NTF_CFG,

    IDX_COMMAND_CHAR,
    IDX_COMMAND_VAL,

    IDX_STATUS_CHAR,
    IDX_STATUS_VAL,
    IDX_STATUS_CFG,

    IDX_NB,
};


typedef struct {
	uint8_t data[MSG_DATA_LEN];
} __attribute__((packed))  BLE_Message_t;

esp_err_t ble_init(char *bt_dev_name);
esp_err_t ble_deinit(void);
esp_err_t ble_reinit(void);
esp_err_t ble_send_data(char *data_str, uint16_t data_len);


#ifdef __cplusplus
}
#endif

#endif /* _BLE_H_ */
