#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "string.h"

#include "esp_gatt_common_api.h"
#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "common.h"
#include "nvs_memory.h"
#include "ble.h"
#include "wifi.h"
#include "thingsboard.h"
#include "sntp_lib.h"

#define GATTS_TAG  "GATTS"
//#define DEBUG_MODE
//#define USE_COMMAND

#define PROFILE_NUM                 1
#define PROFILE_APP_IDX             0
#define APP_ID                      0x16
#define DEVICE_NAME          		"KAIOT_MLN"    //The Device Name Characteristics in GAP
#define SVC_INST_ID	                0

static uint16_t ble_free_cnt = 0;

/// SPP Service
static const uint16_t service_uuid = 0x180D;
/// Characteristic UUID
#define ESP_GATT_UUID_DATA_RECEIVE      0xABF1
#define ESP_GATT_UUID_DATA_NOTIFY       0xABF2

static uint8_t adv_data[MAX_DEVICE_NAME_LEN+10] = {
    /* Flags */
    0x02,0x01,0x06,
    /* Complete List of 16-bit Service Class UUIDs */
    0x03,0x03,0xF0,0xAB,
    /* Complete Local Name in advertising */
    0x0A, 0x09, 'K', 'A', 'I', 'O', 'T', '_', 'M', 'L', 'N'
};
static uint8_t adv_data_len = 18;

static char device_name[MAX_DEVICE_NAME_LEN];

static uint16_t mtu_size = 23;
static uint16_t conn_id = 0xffff;
static esp_gatt_if_t esp_gatts_if = 0xff;
QueueHandle_t data_queue = NULL;

static bool enable_data_ntf = false;
static bool is_connected = false;
static esp_bd_addr_t remote_bda = {0x0,};

static uint16_t handle_table[IDX_NB];

static esp_ble_adv_params_t adv_params = {
    .adv_int_min        = 0x20,
    .adv_int_max        = 0x40,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy  = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
    uint16_t char_handle;
    esp_bt_uuid_t char_uuid;
    esp_gatt_perm_t perm;
    esp_gatt_char_prop_t property;
    uint16_t descr_handle;
    esp_bt_uuid_t descr_uuid;
};

typedef struct receive_data_node{
    int32_t len;
    uint8_t * node_buff;
    struct receive_data_node * next_node;
}receive_data_node_t;

static receive_data_node_t * temp_recv_data_node_p1 = NULL;
static receive_data_node_t * temp_recv_data_node_p2 = NULL;

typedef struct receive_data_buff{
    int32_t node_num;
    int32_t buff_size;
    receive_data_node_t * first_node;
}receive_data_buff_t;

static receive_data_buff_t RecvDataBuff = {
    .node_num   = 0,
    .buff_size  = 0,
    .first_node = NULL
};

static void gatts_profile_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst profile_tab[PROFILE_NUM] = {
    [PROFILE_APP_IDX] = {
        .gatts_cb = gatts_profile_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
};

/*
 *  PROFILE ATTRIBUTES
 ****************************************************************************************
 */

#define CHAR_DECLARATION_SIZE   (sizeof(uint8_t))
static const uint16_t primary_service_uuid = ESP_GATT_UUID_PRI_SERVICE;
static const uint16_t character_declaration_uuid = ESP_GATT_UUID_CHAR_DECLARE;
static const uint16_t character_client_config_uuid = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;

static const uint8_t char_prop_read_notify = ESP_GATT_CHAR_PROP_BIT_READ|ESP_GATT_CHAR_PROP_BIT_NOTIFY;
static const uint8_t char_prop_read_write = ESP_GATT_CHAR_PROP_BIT_WRITE_NR|ESP_GATT_CHAR_PROP_BIT_READ;

///Service - data receive characteristic, read&write without response
static const uint16_t data_receive_uuid = ESP_GATT_UUID_DATA_RECEIVE;
static const uint8_t  data_receive_val[20] = {0x00};

///Service - data notify characteristic, notify&read
static const uint16_t data_notify_uuid = ESP_GATT_UUID_DATA_NOTIFY;
static const uint8_t  data_notify_val[20] = {0x00};
static const uint8_t  data_notify_ccc[2] = {0x00, 0x00};

///Full HRS Database Description - Used to add attributes into the database
static const esp_gatts_attr_db_t gatt_db[IDX_NB] =
{
    //Service Declaration
    [IDX_SVC]                      	=
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&primary_service_uuid, ESP_GATT_PERM_READ,
    sizeof(service_uuid), sizeof(service_uuid), (uint8_t *)&service_uuid}},

    //data receive characteristic Declaration
    [IDX_DATA_RECV_CHAR]            =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
    CHAR_DECLARATION_SIZE,CHAR_DECLARATION_SIZE, (uint8_t *)&char_prop_read_write}},

    //data receive characteristic Value
    [IDX_DATA_RECV_VAL]             	=
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&data_receive_uuid, ESP_GATT_PERM_READ|ESP_GATT_PERM_WRITE,
    DATA_MAX_LEN,sizeof(data_receive_val), (uint8_t *)data_receive_val}},

    //data notify characteristic Declaration
    [IDX_DATA_NOTIFY_CHAR]  =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
    CHAR_DECLARATION_SIZE,CHAR_DECLARATION_SIZE, (uint8_t *)&char_prop_read_notify}},

    //data notify characteristic Value
    [IDX_DATA_NTY_VAL]   =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&data_notify_uuid, ESP_GATT_PERM_READ,
    DATA_MAX_LEN, sizeof(data_notify_val), (uint8_t *)data_notify_val}},

    //data notify characteristic - Client Characteristic Configuration Descriptor
    [IDX_DATA_NTF_CFG]         =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_client_config_uuid, ESP_GATT_PERM_READ|ESP_GATT_PERM_WRITE,
    sizeof(uint16_t),sizeof(data_notify_ccc), (uint8_t *)data_notify_ccc}},

};

static uint8_t find_char_and_desr_index(uint16_t handle)
{
    uint8_t error = 0xff;

    for(int i = 0; i < IDX_NB ; i++){
        if(handle == handle_table[i]){
            return i;
        }
    }

    return error;
}

static bool store_wr_buffer(esp_ble_gatts_cb_param_t *p_data)
{
    temp_recv_data_node_p1 = (receive_data_node_t *)malloc(sizeof(receive_data_node_t));

    if(temp_recv_data_node_p1 == NULL){
        ESP_LOGE(GATTS_TAG, "malloc error %s %d\n", __func__, __LINE__);
        return false;
    }

    if(temp_recv_data_node_p2 != NULL){
        temp_recv_data_node_p2->next_node = temp_recv_data_node_p1;
    }

    temp_recv_data_node_p1->len = p_data->write.len;
    RecvDataBuff.buff_size += p_data->write.len;
    temp_recv_data_node_p1->next_node = NULL;
    temp_recv_data_node_p1->node_buff = (uint8_t *)malloc(p_data->write.len);
    temp_recv_data_node_p2 = temp_recv_data_node_p1;
    memcpy(temp_recv_data_node_p1->node_buff,p_data->write.value,p_data->write.len);
    if(RecvDataBuff.node_num == 0){
        RecvDataBuff.first_node = temp_recv_data_node_p1;
        RecvDataBuff.node_num++;
    }else{
        RecvDataBuff.node_num++;
    }

    return true;
}

static void free_write_buffer(void)
{
    temp_recv_data_node_p1 = RecvDataBuff.first_node;

    while(temp_recv_data_node_p1 != NULL){
        temp_recv_data_node_p2 = temp_recv_data_node_p1->next_node;
        free(temp_recv_data_node_p1->node_buff);
        free(temp_recv_data_node_p1);
        temp_recv_data_node_p1 = temp_recv_data_node_p2;
    }

    RecvDataBuff.node_num = 0;
    RecvDataBuff.buff_size = 0;
    RecvDataBuff.first_node = NULL;
}

static void print_write_buffer(void)
{
    temp_recv_data_node_p1 = RecvDataBuff.first_node;

    while(temp_recv_data_node_p1 != NULL){
    	esp_log_buffer_char(GATTS_TAG, (char *)(temp_recv_data_node_p1->node_buff), temp_recv_data_node_p1->len);
        temp_recv_data_node_p1 = temp_recv_data_node_p1->next_node;
    }
}

static void ble_monitor_task(void * arg) {
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	const uint16_t MAX_BLE_FREE_TIME = 300;
	//const uint16_t MAX_BLE_FREE_TIME = 60;

	while (true) {
		if (isThingsboardConnEstablished() == ESP_OK) {
			if (app_config.is_ble_enable == BLE_ENABLE) {
				if (is_connected == false) {
					if (ble_free_cnt < MAX_BLE_FREE_TIME) {
						ble_free_cnt++;
					}
					else {
						ble_free_cnt = 0;

						esp_err_t err = ble_deinit();
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "ble_deinit error");
						}
						else {
							// update app_config
							app_config.is_ble_enable = BLE_DISABLE;
							err = nvs_memory_set_app_config(&app_config);
							if (err != ESP_OK) {
								ESP_LOGE(GATTS_TAG, "Error! Fail to save app_config to nvs memory!\n");
							}
						}
					}
				}
			}
		}

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}

	vTaskDelete(NULL);
}

static void data_task(void * arg)
{
    BLE_Message_t msg;
    esp_err_t err;
    int cnt;

    vTaskDelay(100 / portTICK_PERIOD_MS);
    // ESP_LOGI(GATTS_TAG, "data_task started!\n");

    for(;;) {
    	vTaskDelay(10 / portTICK_PERIOD_MS);

    	if (data_queue != NULL) {
    		if(xQueueReceive(data_queue, &msg, portMAX_DELAY) == pdTRUE) {
				ESP_LOGD(GATTS_TAG, "Data Value: %s\n", msg.data);
				cJSON *cmd_root = cJSON_Parse((char*)msg.data);
				if (cJSON_GetObjectItem(cmd_root, "btGetDevId")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btGetDevId")->valuestring;
					ESP_LOGD(GATTS_TAG, "btGetDevId cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// get devID from application config
						// prepare data to send to the client
						cJSON *resp_root = cJSON_CreateObject();
						cJSON_AddStringToObject(resp_root, "btRetDevId", app_config.device_id);
						char *devid_string = cJSON_PrintUnformatted(resp_root);
						ESP_LOGD(GATTS_TAG, "devid_string: %s\n",devid_string);
						// send dev_id to client
						err = ble_send_data(devid_string, strlen(devid_string));
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "ble_send_data error");
						}
						cJSON_Delete(resp_root);
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btGetSsid")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btGetSsid")->valuestring;
					ESP_LOGD(GATTS_TAG, "btGetSsid cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// send request to get ssid then send to the client
						err = wifi_start_scan();
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "wifi_start_scan error");
						}
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btSetSsid") && cJSON_GetObjectItem(cmd_root, "btSetPass")) {
					char *ssid = cJSON_GetObjectItem(cmd_root, "btSetSsid")->valuestring;
					char *pass = cJSON_GetObjectItem(cmd_root, "btSetPass")->valuestring;
					ESP_LOGI(GATTS_TAG, "btSetSsid ssid = %s", ssid);
					ESP_LOGI(GATTS_TAG, "btSetPass pass = %s", pass);
					err = wifi_start_with_credentials(ssid, pass);
					if (err != ESP_OK) {
						ESP_LOGE(GATTS_TAG, "wifi_start_with_credentials error");
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btResetWifiProv")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btResetWifiProv")->valuestring;
					ESP_LOGD(GATTS_TAG, "btResetWifiProv cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// send request to reset wifi provision
						err = wifi_reset_provision();
						if (err != ESP_OK) {
							ESP_LOGD(GATTS_TAG, "wifi_reset_provision error");
						}
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btSetOffBle")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btSetOffBle")->valuestring;
					ESP_LOGD(GATTS_TAG, "btSetOffBle cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// notification to client
						cJSON *bleoff_ret_root = cJSON_CreateObject();
						cJSON_AddStringToObject(bleoff_ret_root, "btRetOffBle", "1");

						char *bleoff_ret_string = cJSON_PrintUnformatted(bleoff_ret_root);
						ESP_LOGI(GATTS_TAG, "bleoff_ret_string: %s\n", bleoff_ret_string);
						vTaskDelay(10 / portTICK_PERIOD_MS);
						err = ble_send_data(bleoff_ret_string, strlen(bleoff_ret_string));
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "ble_send_data error: bleoff_ret_string!\n");
						}

						vTaskDelay(1000 / portTICK_PERIOD_MS);
						// send request to off ble
						err = ble_deinit();
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "ble_deinit error");
						}

						// update app_config
						app_config.is_ble_enable = BLE_DISABLE;
						err = nvs_memory_set_app_config(&app_config);
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "Error! Fail to save app_config to nvs memory!\n");
						}

						cJSON_Delete(bleoff_ret_root);
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btGetConnServer")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btGetConnServer")->valuestring;
					ESP_LOGD(GATTS_TAG, "btGetConnServer cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// wait for 30 seconds if server not connected
						for (cnt = 0; cnt < 30; cnt++) {
							if (sv_conn_status == SERVER_CONNECTED) {
								break;
							}
						}

						if (sntp_get_sync_state() != TIME_SYNCHRONIZED) {
							// wait until ntp time is synchronize or 30 seconds of timeout
							xEventGroupWaitBits(system_event_group, NTP_SYNCHRONIZE_EVENT, pdTRUE, pdFALSE, 30000 / portTICK_PERIOD_MS);
						}

						// notify ble client
						cJSON *conn_server_ret_root = cJSON_CreateObject();
						if (sv_conn_status == SERVER_CONNECTED) {
							cJSON_AddStringToObject(conn_server_ret_root, "btRetConnServer", "1");
						}
						else {
							cJSON_AddStringToObject(conn_server_ret_root, "btRetConnServer", "0");
						}
						char *conn_server_ret_string = cJSON_PrintUnformatted(conn_server_ret_root);
						ESP_LOGI(GATTS_TAG, "conn_server_ret_string: %s\n",conn_server_ret_string);
						err = ble_send_data(conn_server_ret_string, strlen(conn_server_ret_string));
						if (err != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "ble_send_data error: conn_server_ret_string!\n");
						}
						cJSON_Delete(conn_server_ret_root);
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btSetProvision")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btSetProvision")->valuestring;
					ESP_LOGD(GATTS_TAG, "btSetProvision cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						err = thingsboard_provision_request();
						if (err == ESP_OK) {
							// notify ble client
							cJSON *set_provision_ret_root = cJSON_CreateObject();
							cJSON_AddStringToObject(set_provision_ret_root, "btRetProvision", app_config.device_token);

							char *set_provision_ret_string = cJSON_PrintUnformatted(set_provision_ret_root);
							ESP_LOGI(GATTS_TAG, "set_provision_ret_string: %s\n",set_provision_ret_string);
							err = ble_send_data(set_provision_ret_string, strlen(set_provision_ret_string));
							if (err != ESP_OK) {
								ESP_LOGE(GATTS_TAG, "ble_send_data error: set_provision_ret_string!\n");
							}
							cJSON_Delete(set_provision_ret_root);
						}
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btSetClaim")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btSetClaim")->valuestring;
					ESP_LOGD(GATTS_TAG, "btSetClaim cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						// send request to off ble
						err = thingsboard_claim_request();
						if (err != ESP_OK) {
							ESP_LOGD(GATTS_TAG, "thingsboard_claim_request error");
						}
						else {
							// wait 5 second before notification
							vTaskDelay(5000 / portTICK_PERIOD_MS);

							// notify secretKey to ble client
							cJSON *claim_ret_root = cJSON_CreateObject();
							cJSON *btRetInfoClaim = cJSON_AddObjectToObject(claim_ret_root, "btRetInfoClaim");
							cJSON_AddStringToObject(btRetInfoClaim, "deviceName", app_config.device_id);
							cJSON_AddStringToObject(btRetInfoClaim, "secretKey", secret_key);
							char *claim_ret_string = cJSON_PrintUnformatted(claim_ret_root);
							ESP_LOGI(GATTS_TAG, "claim_ret_string: %s\n", claim_ret_string);

							err = ble_send_data(claim_ret_string, strlen(claim_ret_string));
							if (err != ESP_OK) {
								ESP_LOGE(GATTS_TAG, "ble_send_data error: secretkey_ret_string!\n");
							}
							cJSON_Delete(claim_ret_root);
						}
					}
				}
				else if (cJSON_GetObjectItem(cmd_root, "btRetClaim")) {
					// current DONOT need to process this
				}
				else if (cJSON_GetObjectItem(cmd_root, "btSetDefValue")) {
					char *cmd = cJSON_GetObjectItem(cmd_root, "btSetDefValue")->valuestring;
					ESP_LOGI(GATTS_TAG, "btSetDefValue cmd = %s", cmd);
					if (strcmp(cmd, "1") == 0) {
						esp_err_t ret = ESP_OK;
						// set default value then publish to platform
						// get UTC0 timestamps to save permanently
						memset(provision_time, 0, TIMESTAMP_MAX_LEN);
						if (nvs_memory_get_provision_time(provision_time) != ESP_OK) {
							// wait time ready in 30 second
							for (cnt = 0; cnt < 30; cnt++) {
								if (sntp_get_sync_state() == TIME_SYNCHRONIZED) {
									break;
								}
								vTaskDelay(1000 / portTICK_PERIOD_MS);
							}

							// neet to confirm that timestamp is get
							if(sntp_get_current_timestamp(provision_time) == ESP_OK) {
								ESP_LOGI(GATTS_TAG, "ProvisionTime: %s\n", provision_time);

								// net to confirm that timestamp is set to memory
								if (nvs_memory_set_provision_time(provision_time) != ESP_OK) {
									ret = ESP_FAIL;
								}
							}
							else {
								ret = ESP_FAIL;
							}
						}
						else {
							ESP_LOGI(GATTS_TAG, "ProvisionTime: %s\n", provision_time);
						}

						// send default value if everything is OK
						if (ret == ESP_OK) {
							ret = thingsboard_set_default_value();
						}

						if (ret != ESP_OK) {
							ESP_LOGE(GATTS_TAG, "thingsboard_set_default_value error");
							// notify to ble client
							cJSON *defval_ret_root = cJSON_CreateObject();
							cJSON_AddStringToObject(defval_ret_root, "btRetDefValue", "0");

							char *defval_ret_string = cJSON_PrintUnformatted(defval_ret_root);
							ESP_LOGI(GATTS_TAG, "defval_ret_string: %s\n", defval_ret_string);

							err = ble_send_data(defval_ret_string, strlen(defval_ret_string));
							if (err != ESP_OK) {
								ESP_LOGE(GATTS_TAG, "ble_send_data error: defval_ret_string!\n");
							}
						}
						else {
							// notify to ble client
							cJSON *defval_ret_root = cJSON_CreateObject();
							cJSON_AddStringToObject(defval_ret_root, "btRetDefValue", "1");

							char *defval_ret_string = cJSON_PrintUnformatted(defval_ret_root);
							ESP_LOGI(GATTS_TAG, "defval_ret_string: %s\n", defval_ret_string);

							err = ble_send_data(defval_ret_string, strlen(defval_ret_string));
							if (err != ESP_OK) {
								ESP_LOGE(GATTS_TAG, "ble_send_data error: defval_ret_string!\n");
							}
							else {
								// Everything is OK, the adding process is completed. Stop bluetooth after waiting 3 seconds
								vTaskDelay(3000 / portTICK_PERIOD_MS);

								err = ble_deinit();
								if (err != ESP_OK) {
									ESP_LOGE(GATTS_TAG, "ble_deinit error");
								}

								// update app_config
								app_config.is_ble_enable = BLE_DISABLE;
								err = nvs_memory_set_app_config(&app_config);
								if (err != ESP_OK) {
									ESP_LOGE(GATTS_TAG, "Error! Fail to save app_config to nvs memory!\n");
								}
							}
							cJSON_Delete(defval_ret_root);
						}
					}
				}
				else {
					/* Invalid request. Not process */
				}

				cJSON_Delete(cmd_root);
			}
    	}


    }
    vTaskDelete(NULL);
}

static void task_init(void)
{
	vTaskDelay(100 / portTICK_PERIOD_MS);

    data_queue = xQueueCreate(2, sizeof(BLE_Message_t));
    xTaskCreate(data_task, "data_task", 3*1024, NULL, 10, NULL);
    xTaskCreate(ble_monitor_task, "ble_monitor_task", 2*1024, NULL, 10, NULL);
}

static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    esp_err_t err;
    ESP_LOGE(GATTS_TAG, "GAP_EVT, event %d\n", event);

    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&adv_params);
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if((err = param->adv_start_cmpl.status) != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTS_TAG, "Advertising start failed: %s\n", esp_err_to_name(err));
        }
        break;
    default:
        break;
    }
}

static void gatts_profile_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    esp_ble_gatts_cb_param_t *p_data = (esp_ble_gatts_cb_param_t *) param;
    uint8_t res = 0xff;

    ESP_LOGI(GATTS_TAG, "event = %x\n",event);

    switch (event) {
    	case ESP_GATTS_REG_EVT:
        	// ESP_LOGI(GATTS_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gap_config_adv_data_raw((uint8_t *)adv_data, adv_data_len);
        	ESP_LOGI(GATTS_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gap_set_device_name(device_name);
        	ESP_LOGI(GATTS_TAG, "%s %d\n", __func__, __LINE__);
        	esp_ble_gatts_create_attr_tab(gatt_db, gatts_if, IDX_NB, SVC_INST_ID);
       	break;
    	case ESP_GATTS_READ_EVT:
            res = find_char_and_desr_index(p_data->read.handle);
            if(res == IDX_STATUS_VAL){
                //TODO:client read the status characteristic
            }
       	 break;
    	case ESP_GATTS_WRITE_EVT: {
    	    res = find_char_and_desr_index(p_data->write.handle);
            if(p_data->write.is_prep == false){
                ESP_LOGI(GATTS_TAG, "ESP_GATTS_WRITE_EVT : handle = %d\n", res);
                if(res == IDX_COMMAND_VAL){

                }
                else if(res == IDX_DATA_NTF_CFG){
                    if((p_data->write.len == 2)&&(p_data->write.value[0] == 0x01)&&(p_data->write.value[1] == 0x00)){
                        enable_data_ntf = true;
                    }else if((p_data->write.len == 2)&&(p_data->write.value[0] == 0x00)&&(p_data->write.value[1] == 0x00)){
                        enable_data_ntf = false;
                    }
                }
                else if(res == IDX_DATA_RECV_VAL){
#ifdef DEBUG_MODE
                    //esp_log_buffer_char(GATTS_TAG,(char *)(p_data->write.value),p_data->write.len);
                    ESP_LOGI(GATTS_TAG, "RECEIVE DATA: %s\n", p_data->write.value);

                    //esp_ble_gatts_send_indicate(esp_gatts_if, conn_id, handle_table[IDX_DATA_NTY_VAL], p_data->write.len, p_data->write.value, false);
#else
                    BLE_Message_t msg;
                    memset(msg.data, 0x0, MSG_DATA_LEN);
                    memcpy(msg.data, p_data->write.value, p_data->write.len);
                    xQueueSend(data_queue, ( void * )&msg, 1000/portTICK_PERIOD_MS);
#endif
                }
                else{
                    //TODO:
                }
            }
            else if((p_data->write.is_prep == true)&&(res == IDX_DATA_RECV_VAL)){
                ESP_LOGI(GATTS_TAG, "ESP_GATTS_PREP_WRITE_EVT : handle = %d\n", res);
                store_wr_buffer(p_data);
            }
      	 	break;
    	}
    	case ESP_GATTS_EXEC_WRITE_EVT:{
    	    ESP_LOGI(GATTS_TAG, "ESP_GATTS_EXEC_WRITE_EVT\n");
    	    if(p_data->exec_write.exec_write_flag){
    	    	print_write_buffer();
    	    	free_write_buffer();
    	    }
    	    break;
    	}
    	case ESP_GATTS_MTU_EVT:
    	    mtu_size = p_data->mtu.mtu;
    	    break;
    	case ESP_GATTS_CONF_EVT:
    	    break;
    	case ESP_GATTS_UNREG_EVT:
        	break;
    	case ESP_GATTS_DELETE_EVT:
        	break;
    	case ESP_GATTS_START_EVT:
        	break;
    	case ESP_GATTS_STOP_EVT:
        	break;
    	case ESP_GATTS_CONNECT_EVT:
    	    conn_id = p_data->connect.conn_id;
    	    esp_gatts_if = gatts_if;
    	    is_connected = true;
    	    memcpy(&remote_bda,&p_data->connect.remote_bda,sizeof(esp_bd_addr_t));

        	break;
    	case ESP_GATTS_DISCONNECT_EVT:
    		ble_free_cnt = 0;
    	    is_connected = false;
    	    enable_data_ntf = false;
    	    esp_ble_gap_start_advertising(&adv_params);
    	    break;
    	case ESP_GATTS_OPEN_EVT:
    	    break;
    	case ESP_GATTS_CANCEL_OPEN_EVT:
    	    break;
    	case ESP_GATTS_CLOSE_EVT:
    	    break;
    	case ESP_GATTS_LISTEN_EVT:
    	    break;
    	case ESP_GATTS_CONGEST_EVT:
    	    break;
    	case ESP_GATTS_CREAT_ATTR_TAB_EVT:{
    	    ESP_LOGI(GATTS_TAG, "The number handle =%x\n",param->add_attr_tab.num_handle);
    	    if (param->add_attr_tab.status != ESP_GATT_OK){
    	        ESP_LOGE(GATTS_TAG, "Create attribute table failed, error code=0x%x", param->add_attr_tab.status);
    	    }
    	    else if (param->add_attr_tab.num_handle != IDX_NB){
    	        ESP_LOGE(GATTS_TAG, "Create attribute table abnormally, num_handle (%d) doesn't equal to HRS_IDX_NB(%d)", param->add_attr_tab.num_handle, IDX_NB);
    	    }
    	    else {
    	        memcpy(handle_table, param->add_attr_tab.handles, sizeof(handle_table));
    	        esp_ble_gatts_start_service(handle_table[IDX_SVC]);
    	    }
    	    break;
    	}
    	default:
    	    break;
    }
}


static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    ESP_LOGI(GATTS_TAG, "EVT %d, gatts if %d\n", event, gatts_if);
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            profile_tab[PROFILE_APP_IDX].gatts_if = gatts_if;
        } else {
            ESP_LOGI(GATTS_TAG, "Reg app failed, app_id %04x, status %d\n",param->reg.app_id, param->reg.status);
            return;
        }
    }

    do {
        int idx;
        for (idx = 0; idx < PROFILE_NUM; idx++) {
            if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gatts_if == profile_tab[idx].gatts_if) {
                if (profile_tab[idx].gatts_cb) {
                    profile_tab[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    } while (0);
}

esp_err_t ble_init(char *bt_dev_name)
{
    esp_err_t err;
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    uint8_t i;
    uint8_t device_name_len;

    ESP_LOGI(GATTS_TAG, "Init BLE peripheral\n");
    ESP_LOGD(GATTS_TAG, "Free memory at start of ble_init %d\n", esp_get_free_heap_size());

    if (strlen(bt_dev_name) > 0) {
    	strcpy(device_name, bt_dev_name);
    }
    else {
    	strcpy(device_name, "KAIOT_MLN");
    }
    device_name_len = strlen(device_name);
    for (i = 0; i < device_name_len; i++) {
		adv_data[9 + i] = device_name[i];
	}
	adv_data[7] = device_name_len + 1;
	adv_data_len = 9 + device_name_len;

    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));

    err = esp_bt_controller_init(&bt_cfg);
    if (err != ESP_OK) {
        ESP_LOGE(GATTS_TAG, "%s init controller failed: %s\n", __func__, esp_err_to_name(err));
        return ESP_FAIL;
    }

    err = esp_bt_controller_enable(ESP_BT_MODE_BLE);
    if (err != ESP_OK) {
        ESP_LOGE(GATTS_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(err));
        return ESP_FAIL;
    }

    err = esp_bluedroid_init();
    if (err != ESP_OK) {
        ESP_LOGE(GATTS_TAG, "%s init bluetooth failed: %s\n", __func__, esp_err_to_name(err));
        return ESP_FAIL;
    }
    err = esp_bluedroid_enable();
    if (err != ESP_OK) {
        ESP_LOGE(GATTS_TAG, "%s enable bluetooth failed: %s\n", __func__, esp_err_to_name(err));
        return ESP_FAIL;
    }

    esp_ble_gatts_register_callback(gatts_event_handler);
    esp_ble_gap_register_callback(gap_event_handler);
    esp_ble_gatts_app_register(APP_ID);

    err = esp_ble_gatt_set_local_mtu(BLE_MTU_SIZE);
	if (err !=ESP_OK ) {
		ESP_LOGE(GATTS_TAG, "%s esp_ble_gatt_set_local_mtu failed: %s\n", __func__, esp_err_to_name(err));
		return ESP_FAIL;
	}

    task_init();

    ESP_LOGD(GATTS_TAG, "Free memory at end of ble_init %d\n", esp_get_free_heap_size());
    return ESP_OK;
}

esp_err_t ble_deinit(void)
{
	esp_err_t err;

	ESP_LOGI(GATTS_TAG, "Deinit BLE peripheral\n");
	ESP_LOGD(GATTS_TAG, "Free memory at start of ble_stop %d", esp_get_free_heap_size());

	err = esp_ble_gap_stop_advertising();
	if (err != ESP_OK) {
		ESP_LOGE(GATTS_TAG, "%s stop gap_advertising failed: %s\n", __func__, esp_err_to_name(err));
		return ESP_FAIL;
	}

	ESP_LOGD(GATTS_TAG, "Free memory at end of ble_stop %d", esp_get_free_heap_size());
	return ESP_OK;
}

esp_err_t ble_reinit(void)
{
	esp_err_t err;

	ESP_LOGI(GATTS_TAG, "Reinit BLE peripheral\n");
	ESP_LOGD(GATTS_TAG, "Free memory at start of ble_reinit %d", esp_get_free_heap_size());

	err = esp_ble_gap_start_advertising(&adv_params);
	if (err != ESP_OK) {
		ESP_LOGE(GATTS_TAG, "%s start gap_advertising failed: %s\n", __func__, esp_err_to_name(err));
		return ESP_FAIL;
	}

	ESP_LOGD(GATTS_TAG, "Free memory at end of ble_reinit %d", esp_get_free_heap_size());
	return ESP_OK;
}

esp_err_t ble_send_data(char *data_str, uint16_t data_len) {
	esp_err_t err;

	if (data_len > 0) {
		if (is_connected == true) {
			err = esp_ble_gatts_send_indicate(esp_gatts_if, conn_id, handle_table[IDX_DATA_NTY_VAL], data_len, (uint8_t*)data_str, false);
			if (err != ESP_OK) {
				ESP_LOGE(GATTS_TAG, "Error (%s) esp_ble_gatts_send_indicate!\n", esp_err_to_name(err));
				return ESP_FAIL;
			}
			else {
				return ESP_OK;
			}
		}
		else {
			return ESP_FAIL;
		}
	}
	else {
		return ESP_FAIL;
	}
}
